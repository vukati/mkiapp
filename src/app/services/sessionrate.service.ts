import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export interface Sessionrate {
  id: string;
  sessionid: string;
  rate: number;
  rater: string;
}

@Injectable({
  providedIn: 'root'
})
export class SessionrateService {
  private todosCollection: AngularFirestoreCollection<Sessionrate>;
  private currentSessionrate: any;
  private todos: Observable<Sessionrate[]>;
 
  constructor(db: AngularFirestore) {
    this.todosCollection = db.collection<Sessionrate>('sessionrates');
 
    this.todos = this.todosCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
  }
 
  getSessionrates() {
    return this.todos;
  }
 
  getSessionrate(id) {
    return this.todosCollection.doc<Sessionrate>(id).valueChanges();
  }
 
  updateSessionrate(sessionrate: Sessionrate, id: string) {
    return this.todosCollection.doc(id).update(sessionrate);
  }
 
  addSessionrate(sessionrate: Sessionrate) {
    return this.todosCollection.add(sessionrate);
  }
 
  removeSessionrate(id) {
    return this.todosCollection.doc(id).delete();
  }


  setCurrent(sessionrate: any) {
    this.currentSessionrate = sessionrate;
  }

  getCurrent() {
    return this.currentSessionrate;
  }
}
