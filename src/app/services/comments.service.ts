import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export interface Comment {
  id: string;
  comment: string;
  name: string;
  email: string;
  date: string;
  eventid: string;
}
 
@Injectable({
  providedIn: 'root'
})
export class CommentService {
  private todosCollection: AngularFirestoreCollection<Comment>;
 
  private todos: Observable<Comment[]>;

  private currentEvent: any;
 
  constructor(db: AngularFirestore) {
    this.todosCollection = db.collection<Comment>('comments');
 
    this.todos = this.todosCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
  }
 
  getComments() {
    return this.todos;
  }
 
  getComment(title) {
    return this.todosCollection.doc<Comment>(title).valueChanges();
  }
 
  updateComment(comment: Comment, title: string) {
    return this.todosCollection.doc(title).update(comment);
  }
 
  addComment(comment: Comment) {
    return this.todosCollection.add(comment);
  }
 
  removeComment(title) {
    return this.todosCollection.doc(title).delete();
  }

  setCurrent(event: any) {
    this.currentEvent = event;
  }

  getCurrent() {
    return this.currentEvent;
  }
}
