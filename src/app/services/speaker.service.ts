import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export interface Speaker {
  names: string;
  institution: string;
  biography: string;
  phone: string;
  email: string;
  linkedin: string;
  webpage: string;
  eventid: string;
  picture: string;
}

@Injectable({
  providedIn: 'root'
})
export class SpeakerService {
  private todosCollection: AngularFirestoreCollection<Speaker>;
  private currentSpeaker: any;
  private todos: Observable<Speaker[]>;
 
  constructor(db: AngularFirestore) {
    this.todosCollection = db.collection<Speaker>('speakers');
 
    this.todos = this.todosCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
  }
 
  getSpeakers() {
    return this.todos;
  }
 
  getSpeaker(id) {
    return this.todosCollection.doc<Speaker>(id).valueChanges();
  }
 
  updateSpeaker(speaker: Speaker, id: string) {
    return this.todosCollection.doc(id).update(speaker);
  }
 
  addSpeaker(speaker: Speaker) {
    return this.todosCollection.add(speaker);
  }
 
  removeSpeaker(id) {
    return this.todosCollection.doc(id).delete();
  }


  setCurrent(speaker: any) {
    this.currentSpeaker = speaker;
  }

  getCurrent() {
    return this.currentSpeaker;
  }
}
