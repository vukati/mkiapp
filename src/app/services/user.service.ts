import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export interface Person {
  id: string;
  name: string;
  surname: string;
  phone: string;
  email: string;
  image: string;
  role?: string;
  institution?: string; 
}

@Injectable({
  providedIn: 'root'
})
export class PersonService {
  private todosCollection: AngularFirestoreCollection<Person>;
  private currentPerson: any;
  private todos: Observable<Person[]>;

  private usersCollection: AngularFirestoreCollection<Person>;
  private users: Observable<Person[]>;

  private usersCollection1: AngularFirestoreCollection<Person>;
  private users1: Observable<Person[]>;
 
  constructor(db: AngularFirestore) {
    this.todosCollection = db.collection<Person>('users');
 
    this.todos = this.todosCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );




    this.usersCollection = db.collection<Person>('users');

    this.users = this.usersCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );




    this.usersCollection1 = db.collection<Person>('users');

    this.users1 = this.usersCollection1.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
  }
 
  getPersons() {
    return this.todos;
  }

  getUser() {
    return this.users;
  }


  getUser1() {
    return this.users1;
  }


  getPerson(id) {
    return this.todosCollection.doc<Person>(id).valueChanges();
  }
 
  updatePerson(person: Person, id: string) {
    return this.todosCollection.doc(id).update(person);
  }
 
  addPerson(person: Person) {
    return this.todosCollection.add(person);
  }
 
  removePerson(id) {
    return this.todosCollection.doc(id).delete();
  }


  setCurrent(person: any) {
    this.currentPerson = person;
  }

  getCurrent() {
    return this.currentPerson;
  }





}
