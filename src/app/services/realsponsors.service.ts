import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export interface Realsponsor {
  title: string;
  content: string;
  image: string;
}
 
@Injectable({
  providedIn: 'root'
})
export class RealsponsorService {
  private todosCollection: AngularFirestoreCollection<Realsponsor>;
 
  private todos: Observable<Realsponsor[]>;

  private todosCollection1: AngularFirestoreCollection<Realsponsor>;

  private todos1: Observable<Realsponsor[]>;

  realsponsors: any[];

  private currentRealsponsor: any;
 
  constructor(db: AngularFirestore) {
    this.todosCollection = db.collection<Realsponsor>('realsponsors');
 
    this.todos = this.todosCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );






    this.todosCollection1 = db.collection<Realsponsor>('realsponsors');

    this.todos1 = this.todosCollection1.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
  }
 
  getRealsponsors() {
    return this.todos;
  }

  getRealsponsors1() {
    return this.todos1;
  }
 
  getRealsponsor(title) {
    return this.todosCollection.doc<Realsponsor>(title).valueChanges();
  }
 
  updateRealsponsor(realsponsor: Realsponsor, title: string) {
    return this.todosCollection.doc(title).update(realsponsor);
  }
 
  addRealsponsor(realsponsor: Realsponsor) {
    return this.todosCollection.add(realsponsor);
  }
 
  removeRealsponsor(title) {
    return this.todosCollection.doc(title).delete();
  }



  setCurrent(realsponsor: any) {
    this.currentRealsponsor = realsponsor;
  }

  getCurrent() {
    return this.currentRealsponsor;
  }
}
