import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export interface Schedule {
  names: string;
  institution: string;
  biography: string;
  phone: string;
  email: string;
  linkedin: string;
  webpage: string;
  eventid: string;
  picture: string;
}

@Injectable({
  providedIn: 'root'
})
export class ScheduleService {
  private todosCollection: AngularFirestoreCollection<Schedule>;
 
  private todos: Observable<Schedule[]>;
 
  constructor(db: AngularFirestore) {
    this.todosCollection = db.collection<Schedule>('schedules');
 
    this.todos = this.todosCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
  }
 
  getSchedules() {
    return this.todos;
  }
 
  getSchedule(id) {
    return this.todosCollection.doc<Schedule>(id).valueChanges();
  }
 
  updateSchedule(schedule: Schedule, id: string) {
    return this.todosCollection.doc(id).update(schedule);
  }
 
  addSchedule(schedule: Schedule) {
    return this.todosCollection.add(schedule);
  }
 
  removeSchedule(id) {
    return this.todosCollection.doc(id).delete();
  }
}
