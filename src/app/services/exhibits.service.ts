import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export interface Exhibit {
  title: string;
  content: string;
  image: string;
}
 
@Injectable({
  providedIn: 'root'
})
export class ExhibitService {
  private todosCollection: AngularFirestoreCollection<Exhibit>;
 
  private todos: Observable<Exhibit[]>;

  private todosCollection1: AngularFirestoreCollection<Exhibit>;

  private todos1: Observable<Exhibit[]>;

  exhibits: any[];

  private currentExhibit: any;
 
  constructor(db: AngularFirestore) {
    this.todosCollection = db.collection<Exhibit>('exhibits');
 
    this.todos = this.todosCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );






    this.todosCollection1 = db.collection<Exhibit>('exhibits');

    this.todos1 = this.todosCollection1.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
  }
 
  getExhibits() {
    return this.todos;
  }

  getExhibits1() {
    return this.todos1;
  }
 
  getExhibit(title) {
    return this.todosCollection.doc<Exhibit>(title).valueChanges();
  }
 
  updateExhibit(exhibit: Exhibit, title: string) {
    return this.todosCollection.doc(title).update(exhibit);
  }
 
  addExhibit(exhibit: Exhibit) {
    return this.todosCollection.add(exhibit);
  }
 
  removeExhibit(title) {
    return this.todosCollection.doc(title).delete();
  }



  setCurrent(exhibit: any) {
    this.currentExhibit = exhibit;
  }

  getCurrent() {
    return this.currentExhibit;
  }
}
