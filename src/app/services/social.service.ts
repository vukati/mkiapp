import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export interface Social {
  id: string;
  title: string;
  link: string;
  date: string;
  type: string;
}
 
@Injectable({
  providedIn: 'root'
})
export class SocialService {
  private todosCollection: AngularFirestoreCollection<Social>;
 
  private todos: Observable<Social[]>;

  private todosCollection1: AngularFirestoreCollection<Social>;

  private todos1: Observable<Social[]>;
 
  constructor(db: AngularFirestore) {
    this.todosCollection = db.collection<Social>('socials');
 
    this.todos = this.todosCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );






    this.todosCollection1 = db.collection<Social>('socials');

    this.todos1 = this.todosCollection1.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
  }
 
  getSocials() {
    return this.todos;
  }

  getSocials1() {
    return this.todos1;
  }
 
  getSocial(title) {
    return this.todosCollection.doc<Social>(title).valueChanges();
  }
 
  updateSocial(social: Social, title: string) {
    return this.todosCollection.doc(title).update(social);
  }
 
  addSocial(social: Social) {
    return this.todosCollection.add(social);
  }
 
  removeSocial(title) {
    return this.todosCollection.doc(title).delete();
  }
}
