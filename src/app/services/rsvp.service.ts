import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export interface Rsvp {
  id: string;
  sessionid: string;
  user: string;
}
 
@Injectable({
  providedIn: 'root'
})
export class RsvpService {
  private todosCollection: AngularFirestoreCollection<Rsvp>;
 
  private todos: Observable<Rsvp[]>;

  private todosCollection1: AngularFirestoreCollection<Rsvp>;

  private todos1: Observable<Rsvp[]>;
 
  constructor(db: AngularFirestore) {
    this.todosCollection = db.collection<Rsvp>('rsvps');
 
    this.todos = this.todosCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );






    this.todosCollection1 = db.collection<Rsvp>('rsvps');

    this.todos1 = this.todosCollection1.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
  }
 
  getRsvps() {
    return this.todos;
  }

  getRsvps1() {
    return this.todos1;
  }
 
  getRsvp(title) {
    return this.todosCollection.doc<Rsvp>(title).valueChanges();
  }
 
  updateRsvp(rsvp: Rsvp, title: string) {
    return this.todosCollection.doc(title).update(rsvp);
  }
 
  addRsvp(rsvp: Rsvp) {
    return this.todosCollection.add(rsvp);
  }
 
  removeRsvp(title) {
    return this.todosCollection.doc(title).delete();
  }
}
