import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export interface Event {
  title: string;
  date: string,
  startdatetime: string;
  enddatetime: string;
  description: string;
  location: string;
  favorite: string;
  reminder: boolean;
  owner: string;
  id: string;
  rsvps: string;
  ratings: number;
  rsvpd: boolean;
  rate: number;
  rates: number;

  sessions: EventSession[];
  documents: any[];
}

export interface EventSession {
  title: string;
  description: string;
  date: string,
  startdatetime: string;
  enddatetime: string;
  ratings: number;
  rate: number;
  rates: number;
}
 
@Injectable({
  providedIn: 'root'
})
export class EventService {

  private todosCollectionhome: AngularFirestoreCollection<Event>;

  public todoshome: Observable<Event[]>;

  private todosCollection: AngularFirestoreCollection<Event>;
 
  private todos: Observable<Event[]>;

  private sessionsCollection: AngularFirestoreCollection<Event>;

  private sessions: Observable<Event[]>;


  private todosCollection1: AngularFirestoreCollection<Event>;

  private todos1: Observable<Event[]>;

  private currentEvent: any;

  constructor(db: AngularFirestore) {
    this.todosCollection = db.collection<Event>('events');
 
    this.todos = this.todosCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );


    this.sessionsCollection = db.collection<Event>('sessions');

    this.sessions = this.sessionsCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );


    this.todosCollectionhome = db.collection<Event>('events');

    this.todoshome = this.todosCollectionhome.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );



    this.todosCollection1 = db.collection<Event>('events');

    this.todos1 = this.todosCollection1.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
  }
 
  getEvents() {
    return this.todos;
  }


  getSessions() {
    return this.sessions;
  }

  gethomeEvents() {
    return this.todoshome;
  }

  getEvents1() {
    return this.todos1;
  }
 
  getEvent(id) {
    return this.todosCollection.doc<Event>(id).valueChanges();
  }
 
  updateEvent(event: Event, id: string) {
    return this.todosCollection.doc(id).update(event);
  }
 
  addEvent(event: Event) {
    return this.todosCollection.add(event);
  }
 
  removeEvent(id) {
    return this.todosCollection.doc(id).delete();
  }

  removeSession(id) {
    return this.sessionsCollection.doc(id).delete();
  }

  setCurrent(event: any) {
    this.currentEvent = event;
  }

  getCurrent() {
    return this.currentEvent;
  }

}
