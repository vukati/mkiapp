import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export interface Banner {
  title: string;
  content: string;
  image: string;
}
 
@Injectable({
  providedIn: 'root'
})
export class BannerService {
  private todosCollection: AngularFirestoreCollection<Banner>;
 
  private todos: Observable<Banner[]>;

  private todosCollection1: AngularFirestoreCollection<Banner>;

  private todos1: Observable<Banner[]>;
 
  constructor(db: AngularFirestore) {
    this.todosCollection = db.collection<Banner>('banners');
 
    this.todos = this.todosCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );






    this.todosCollection1 = db.collection<Banner>('banners');

    this.todos1 = this.todosCollection1.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
  }
 
  getBanners() {
    return this.todos;
  }

  getBanners1() {
    return this.todos1;
  }
 
  getBanner(title) {
    return this.todosCollection.doc<Banner>(title).valueChanges();
  }
 
  updateBanner(banner: Banner, title: string) {
    return this.todosCollection.doc(title).update(banner);
  }
 
  addBanner(banner: Banner) {
    return this.todosCollection.add(banner);
  }
 
  removeBanner(title) {
    return this.todosCollection.doc(title).delete();
  }
}
