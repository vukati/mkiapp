import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export interface Speakerrate {
  id: string;
  speakerid: string;
  rate: number;
  rater: string;
}

@Injectable({
  providedIn: 'root'
})
export class SpeakerrateService {
  private todosCollection: AngularFirestoreCollection<Speakerrate>;
  private currentSpeakerrate: any;
  private todos: Observable<Speakerrate[]>;
 
  constructor(db: AngularFirestore) {
    this.todosCollection = db.collection<Speakerrate>('speakerrates');
 
    this.todos = this.todosCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
  }
 
  getSpeakerrates() {
    return this.todos;
  }
 
  getSpeakerrate(id) {
    return this.todosCollection.doc<Speakerrate>(id).valueChanges();
  }
 
  updateSpeakerrate(speakerrate: Speakerrate, id: string) {
    return this.todosCollection.doc(id).update(speakerrate);
  }
 
  addSpeakerrate(speakerrate: Speakerrate) {
    return this.todosCollection.add(speakerrate);
  }
 
  removeSpeakerrate(id) {
    return this.todosCollection.doc(id).delete();
  }


  setCurrent(speakerrate: any) {
    this.currentSpeakerrate = speakerrate;
  }

  getCurrent() {
    return this.currentSpeakerrate;
  }
}
