import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import { File, FileEntry } from '@ionic-native/file/ngx';
import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
declare let window: any;

@Injectable({
  providedIn: 'root'
})
export class EventDataService {

  sbaList: any;

  constructor() {
    this.sbaList = firebase.database().ref('/sbalist');
  }
  makeFileIntoBlob(_imagePath, name, type) {

    // INSTALL PLUGIN - cordova plugin add cordova-plugin-file
    return new Promise((resolve, reject) => {
      window.resolveLocalFileSystemURL(_imagePath, (fileEntry: FileEntry) => {

        fileEntry.file((resFile) => {

          var reader = new FileReader();
          reader.onloadend = (evt: any) => {
            var imgBlob: any = new Blob([evt.target.result], { type: type });
            imgBlob.name = name;
            resolve(imgBlob);
          };

          reader.onerror = (e) => {
            alert('Failed file read: ' + e.toString());
            reject(e);
          };

          reader.readAsArrayBuffer(resFile);
        });
      });
    });
  }

  getfilename(filestring) {

    let file
    file = filestring.replace(/^.*[\\\/]/, '')
    return file;
  }

  getfileext(filestring) {
    let file = filestring.substr(filestring.lastIndexOf('.') + 1);
    return file;
  }
  getRequestFiles(sbaid): any {

    return this.sbaList.child('sbafiles');

  }



  addAssignmentFile(file: any): any {

    return this.sbaList.child(file.filename)
      //Saves the file to storage
      .put(file.blob, { contentType: file.type }).then((savedFile) => {
        //Gets the file url and saves it in the database
        this.sbaList.child('sbafiles').push({
          file: savedFile.downloadURL,
          name: file.filename,
          ext: file.fileext,
          type: file.type
        });
      })
  }
}
