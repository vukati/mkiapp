import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { RouteReuseStrategy } from '@angular/router';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule, AngularFireDatabase } from '@angular/fire/database';
import { AngularFirestore } from '@angular/fire/firestore';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { IonicStorageModule } from '@ionic/storage';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { DATA_SERVICE, DataServiceType, firebaseConfig } from 'src/config';
import { HttpDataService } from './services/database/http-data.service';
import { FirebaseDataService } from './services/database/firebase-data.service';
import { DataService } from './services/database/data.service';
import { TabsPageModule } from './pages/tabs/tabs.module';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { Camera } from '@ionic-native/camera';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { File } from '@ionic-native/file/ngx';
import { AngularFireStorage } from '@angular/fire/storage';

export function getDataService(http: HttpClient, db: AngularFireDatabase) {
  switch (DATA_SERVICE) {
    case DataServiceType.http:
      return new HttpDataService(http);
    case DataServiceType.firebase:
      return new FirebaseDataService(db);
    default:
      throw new Error('Unknown service');
  }
}

export function provideStorage() {
  return new Storage();
  // return new Storage(['sqlite', 'websql', 'indexeddb'], { name: '__mydb' } /* optional config */);
}

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    TabsPageModule,
    HttpClientModule,
    IonicStorageModule.forRoot(),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),

  ],
  providers: [
    AngularFirestore,
    StatusBar,
    SplashScreen,
    File,
    //Camera,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    { provide: DataService, useFactory: getDataService, deps: [HttpClient, AngularFireDatabase] },
    { provide: Storage, useFactory: provideStorage },
    LocalNotifications,
    InAppBrowser,
    AngularFireStorage
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
