
export interface BannerOptions {
  title: string;
  content: string;
  image: string;
}
