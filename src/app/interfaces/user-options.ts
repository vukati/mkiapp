
export interface UserOptions {
  name: string;
  surname: string;
  phone: string;
  email: string;
  password: string;
  institution: string;
  position: string;
}

export interface EventOptions {
  title: string;
  date: string,
  startdatetime: string;
  enddatetime: string;
  description: string;
  location: string;
  favorite: string;
  reminder: boolean;
  owner: string;
  id: string;
  sessions: EventSession[];
  documents: any[];
}

export interface EventSession {
  title: string;
  description: string;
  date: string,
  startdatetime: string;
  enddatetime: string;
  ratings: number;
  rate: number;
  rates: number;
}
export interface SpeakerOptions {
  names: string;
  institution: string;
  biography: string;
  phone: string;
  email: string;
  linkedin: string;
  webpage: string;
  eventid: string;
  picture: string;
}

export interface ScheduleOptions {
  schedules: ScheduleDayOptions[];
}

export interface ScheduleDayOptions {
  date: string;
  sessions: SessionOptions[];
}

export interface SessionOptions {
  title: string;
  subsessions: SubSessionOptions[];
}

export interface SubSessionOptions {
  timeStart: string;
  timeEnd: string;
  location: string;
  title: string;
  description: string;
  reminder: boolean;
  speakers: string[];
}


export interface CommentOptions {
  id: string;
  comment: string;
  name: string;
  email: string;
  date: string;
  eventid: string;
}


export interface UserOptions1 {
  name: string;
  surname: string;
  phone: string;
  email: string;
  password: string;
  institution: string;
  position: string;
  facebook: string;
  twitter: string;
  webpage: string;
  description: string;
}
