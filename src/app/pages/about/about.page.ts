import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { DataService } from 'src/app/services/database/data.service';
import { EmailService } from 'src/app/services/common/email.service';
import { MapsService } from 'src/app/services/common/maps.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-about',
  templateUrl: './about.page.html',
  styleUrls: ['./about.page.scss'],
})
export class AboutPage implements OnInit {

  about: any;

  constructor(
    private dataService: DataService,
    private emailService: EmailService,
    private mapService: MapsService,
    private titleService: Title,
    private router: Router,
  ) { }

  ngOnInit() {
    this.titleService.setTitle('About');
    this.dataService.getAbout().then((about) => {
      this.about = about;
    });
  }

  sendEmail(email: string) {
    this.emailService.sendEmail(email);
  }

  findUs() {
    this.router.navigate(['tabs/map']);
  }
}
