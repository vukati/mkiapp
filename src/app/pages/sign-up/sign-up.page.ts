import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { NavController } from '@ionic/angular';
import { UserDataService } from 'src/app/services/common/user-data.service';
import { UserOptions } from 'src/app/interfaces/user-options';
import { EventsService } from 'src/app/services/common/events.service';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Storage } from '@ionic/storage';
import { PersonService } from 'src/app/services/user.service';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.page.html',
  styleUrls: ['./sign-up.page.scss'],
})
export class SignUpPage implements OnInit {

  signup: UserOptions = { name: '', surname: '', phone: '', email: '', password: '', institution: '', position: '' };
  submitted = false;
  signupError: string;

  constructor(
    private navCtrl: NavController,
    private userData: UserDataService,
    private titleService: Title,
    private eventsService: EventsService,
    public afs: AngularFirestore,
    private storage: Storage,
    private userService: PersonService,
  ) { }

  ngOnInit() {
    this.titleService.setTitle('Signup');
  }

  onSignup(form: NgForm) {
    this.submitted = true;
    this.signupError = '';

    if (form.valid) {
      this.userData.signUp(this.signup)

        .then(
        () => {
            this.addUser(this.signup);
          this.storage.set('uemail', this.signup.email);
          this.storage.set('email', this.signup.email);
          this.storage.set('password', this.signup.password);
          
            this.navCtrl.navigateRoot('/tabs/home');
            this.eventsService.userSignUp(true);
          },
          (error) => this.signupError = error.message,
        );
    }
  }

  getUser(email) {
    this.userService.getUser().subscribe(async res => {
      this.storage.set('user', res.filter(i => i.email == email)[0]);

    });
  }

  addUser(signup) {
    return new Promise<any>((resolve, reject) => {
      this.afs.collection('/users').add({
        name: signup.name,
        surname: signup.surname,
        phone: signup.phone,
        email: signup.email,
        institution: signup.institution,
        position: signup.position,
        image: '',
        role: '2',
      })
        .then(
          (res) => {
            resolve(res)
            this.getUser(this.signup.email);
          },
          err => reject(err)
        )
    })
  }
}
