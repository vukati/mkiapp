import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { NavController } from '@ionic/angular';
import { UserDataService } from 'src/app/services/common/user-data.service';
import { UserOptions } from 'src/app/interfaces/user-options';
import { EventsService } from 'src/app/services/common/events.service';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable, Subscription } from 'rxjs';
import { ChatService } from 'src/app/services/chat.service';

import * as firebase from 'firebase';
import { IonItemSliding, ToastController } from '@ionic/angular';
@Component({
  selector: 'app-sponsors',
  templateUrl: './sponsors.component.html',
  styleUrls: ['./sponsors.component.scss'],
})
export class SponsorsComponent implements OnInit {

  chat: any[] = [];
  chat2: any[] = [];
  queryText = '';

  signup: UserOptions = { name: '', surname: '', phone: '', email: '', password: '', institution: '', position: '' };
  submitted = false;
  signupError: string;

  constructor(
    private navCtrl: NavController,
    private userData: UserDataService,
    private titleService: Title,
    private eventsService: EventsService,
    public afs: AngularFirestore,
    private chatService: ChatService,
    private toastCtrl: ToastController,
  ) { }

  ngOnInit() {
    this.titleService.setTitle('Admin Users');
    this.updateChat();
  }

  private chatsubscription: Subscription = new Subscription();

  ngOnDestroy() {
    this.chatsubscription.unsubscribe();
  }


  updateChat() {
    this.chatsubscription.unsubscribe();
    this.chatsubscription = this.chatService.getChatsUsers().subscribe(async (chat: any[]) => {
      //for (var a = 0; a < chat.length; a++) {
      //  if (chat[a].image.length > 0) {
      //    const aaaa = await this.getImgFromServer(chat[a].image);
      //    chat[a].image = aaaa;
      //  }

      //}

      chat = chat.filter(item => item.role == '1')

      this.chat = chat;
      this.chat2 = chat; 
    });
    
  }

  async getImgFromServer(imgName: string) {
    let img;
    let dowvnloadIMG;
    var retVal = '';
    img = firebase.storage().ref("/pictures/" + imgName).getDownloadURL();
    let ref = firebase.storage().ref();
    const imgRef = ref.child("/pictures/" + imgName);
    const downloadURL = await imgRef.getDownloadURL()

    retVal = downloadURL;

    return retVal
  }


  onSignup(form: NgForm) {
    this.submitted = true;
    this.signupError = '';

    if (form.valid) {
      this.userData.signUp(this.signup)

        .then(
          () => {
            this.addUser(this.signup); 
          },
          (error) => this.signupError = error.message,
        );
    }
  }


  async addUser(signup) {

    const toast = await this.toastCtrl.create({
      message: 'User added Successfully! You are now logged in as the new Admin, log out and log back in as yourself to resume your tasks',
      duration: 9000,
    });
    toast.present();
    return new Promise<any>((resolve, reject) => {
      this.afs.collection('/users').add({
        name: signup.name,
        surname: signup.surname,
        phone: signup.phone,
        email: signup.email,
        image: '',
        role: '1',
      })
        .then(
          (res) => {
            resolve(res)
          },
          err => reject(err)
        )
    })

  }
   
}
