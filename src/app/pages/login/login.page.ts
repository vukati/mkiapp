import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { NavController } from '@ionic/angular';
import { UserOptions } from 'src/app/interfaces/user-options';
import { UserDataService } from 'src/app/services/common/user-data.service';
import { EventsService } from 'src/app/services/common/events.service';
import { Storage } from '@ionic/storage';
import { Observable, Subscription } from 'rxjs';
import { ChatService } from 'src/app/services/chat.service';
import { PersonService } from 'src/app/services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  login: UserOptions = { name: '', surname: '', phone: '', email: '', password: '', institution: '', position: '' };
  submitted = false;
  loginError: string;

  constructor(
    private userData: UserDataService,
    private navCtrl: NavController,
    private router: Router,
    private titleService: Title,
    private eventsService: EventsService,
    private storage: Storage,
    private chatService: ChatService,
    private userService: PersonService,
  ) { }

  ngOnInit() {
    this.titleService.setTitle('Login'); 
    this.storage.get('email').then((data) => { this.login.email = data; });
    this.storage.get('password').then((data) => { this.login.password = data; });
  }

  onLogin(form: NgForm) {
    this.loginError = '';
    this.submitted = true;

    if (form.valid) {

       this.userData.signInWithEmail(this.login).then(
         () => {
          this.storage.set('uemail', this.login.email);
          this.storage.set('email', this.login.email);
           this.storage.set('password', this.login.password);
           this.getUser(this.login.email);
          this.navCtrl.navigateRoot('/tabs/home');
          this.eventsService.userLogIn(true);
        },
        (error) => this.loginError = error.message,
      );
    }
  }

  private chatsubscription: Subscription = new Subscription();

  ngOnDestroy() {
    this.chatsubscription.unsubscribe();
  }

  getUser(email) {
    this.userService.getUser().subscribe(async res => {
      this.storage.set('user', res.filter(i => i.email == email)[0]);

    });
  }

  resetPassword() {
    this.router.navigate(['reset']);
  }

  onSignup() {
    this.router.navigate(['sign-up']);
  }
}
