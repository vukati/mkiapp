import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { NavController } from '@ionic/angular';
import { UserDataService } from 'src/app/services/common/user-data.service';
import { UserOptions, EventOptions } from 'src/app/interfaces/user-options';
import { EventsService } from 'src/app/services/common/events.service';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { IonItemSliding, ToastController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';

@Component({
  selector: 'app-addschedule',
  templateUrl: './addschedule.page.html',
  styleUrls: ['./addschedule.page.scss'],
})
export class AddSchedulePage implements OnInit {

  signup: EventOptions = { title: '', date: '', startdatetime: '', enddatetime: '', description: '', location: '', favorite: 'no', reminder: false, owner: 'all', id: '', sessions: [], documents: []};
  submitted = false;
  signupError: string;
  useremail: string = '';
  constructor(
    private navCtrl: NavController,
    private userData: UserDataService,
    private titleService: Title,
    private eventsService: EventsService,
    public afs: AngularFirestore,
    private toastCtrl: ToastController,
    private router: Router,
    private localNotifications: LocalNotifications
  ) { }

  ngOnInit() {
    this.titleService.setTitle('Add New Session');
    this.userData.getEmail().subscribe(async res => {
      this.useremail = res;

    });
  }

  async onSignup(form: NgForm) {
    this.submitted = true;
    this.signupError = '';
  
    if (form.valid) {
      this.addUser(this.signup)

      var yr = new Date(this.signup.date).getFullYear();
      var mon = new Date(this.signup.date).getMonth();
      var day = new Date(this.signup.date).getDay();

      var hr = new Date(this.signup.startdatetime).getHours();
      var min = new Date(this.signup.startdatetime).getMinutes();
      var secs = new Date(this.signup.startdatetime).getSeconds();
      var mil = new Date(this.signup.startdatetime).getMilliseconds();

      var remdate = new Date();
      remdate.setDate(new Date(this.signup.date).getDate())
      remdate.setTime(new Date(this.signup.startdatetime).getTime())


      console.log(new Date(remdate));
      console.log(remdate);
      console.log(new Date(new Date().getTime() + 60))

      if (this.signup.reminder) {
        this.localNotifications.schedule({
          text: this.signup.title,
          trigger: { at: remdate},
          led: 'FF0000',
          sound: null
        });
      }
      
      this.signup.title = '';
      this.signup.description = '';
      this.signup.location = '';
      this.signup.startdatetime = '';
      this.signup.enddatetime = '';

      const toast = await this.toastCtrl.create({
        message: 'Event added Successfully!',
        duration: 3000,
      });
      toast.present();

      this.router.navigate(['tabs/schedule']);
    }
  }

  addUser(signup) {
    return new Promise<any>((resolve, reject) => {
      this.afs.collection('/sessions').add({
        title: signup.title,
        location: signup.location,
        description: signup.description,
        date: signup.date,
        startdatetime: signup.startdatetime,
        enddatetime: signup.enddatetime,
        favorite: signup.favorite,
        reminder: signup.reminder,
        owner: this.useremail,
      })
        .then(
          (res) => {
            resolve(res)
          },
          err => reject(err)
        )
    })
  }

}
