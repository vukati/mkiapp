import { HomeService } from '../home.service';
import { EventService } from 'src/app/services/events.service';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { NavController } from '@ionic/angular';
import { UserDataService } from 'src/app/services/common/user-data.service';
import { UserOptions, EventOptions, CommentOptions } from 'src/app/interfaces/user-options';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { IonItemSliding, ToastController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { CommentService, Comment } from 'src/app/services/comments.service'
import { SpeakerService, Speaker } from 'src/app/services/speaker.service';
import { SessionrateService } from 'src/app/services/sessionrate.service';
import { Observable, Subscription } from 'rxjs';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { Storage } from '@ionic/storage';
import { Day, Session, SubSession } from 'src/app/interfaces/sessions';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { first, map } from 'rxjs/operators';

declare var google: any;
declare var cordova: any;

@Component({
  selector: 'app-home-detail',
  templateUrl: './home-detail.component.html',
  styleUrls: ['./home-detail.component.scss'],
  providers: [SocialSharing]
 })
export class HomeDetailComponent implements OnInit {
  session: any = [];
  @ViewChild('mapCanvas', { static: false }) mapElement: ElementRef;
  sessioncomment: any = [];
  useremail: string = '';
  usernames: string = '';
  segment: string = 'Details';
  comment: CommentOptions = { id: '', name: '', comment: '', email: '', date: '', eventid: '' };
  submitted = false;
  signupError: string;
  speakers: any[] = [];
  speakers2: any[] = [];
  sessions_items: any[] = [];
  files: any[] = [];

  constructor(
    private scheduleService: HomeService,
    private activatedRoute: ActivatedRoute,
    private commentService: CommentService,
    private navCtrl: NavController,
    private userData: UserDataService,
    private titleService: Title,
    private eventsService: EventService,
    public afs: AngularFirestore,
    private toastCtrl: ToastController,
    private router: Router,
    private speakersService: SpeakerService,
    private sessionrateService: SessionrateService,
    private socialSharing: SocialSharing,
    private storage: Storage,
    private route: ActivatedRoute,
    private iab: InAppBrowser
  ) { }
  comments_items: Comment[];
  c: Comment = { id: ' ', comment: ' ', name: ' ', email: ' ', date: ' ', eventid: ' ' }
  s: Speaker = { names: '', institution: '', biography: '', phone: '', email: '', linkedin: '', webpage: '', eventid: '', picture: '' }
  rsvpuser: string = '';
  ngOnInit() {
    this.session = this.eventsService.getCurrent();
    this.updateRating(this.session.ratings, this.session.rates);
    this.c.date = Date.now().toString();
    this.s.names = Date.now().toString();
    this.commentService.updateComment(this.c, 'fdiRvpZyc864DjlwTRd8');
    this.speakersService.updateSpeaker(this.s, 'tKnIDk1qyDXGwWXSWmLj');

    this.sessions_items = this.session.sessions;
    this.files = this.session.documents;

     if (!this.session) {
       this.init();
    }

    this.userData.getEmail().subscribe(async res => {
      this.useremail = res;
    });

    this.storage.get('user').then((data) => { console.log(data); this.usernames = data.name + ' ' + data.surname, this.rsvpuser = data.name + ' ' + data.surname + ',  ' + data.position +' from ' + data.institution });

    this.updateComment();
    this.speakersService.getSpeakers().subscribe((speakers: any[]) => {
      speakers = speakers.filter(i => i.eventid == this.session.id)
      this.speakers = this.sortByKey(speakers, 'names');
      this.speakers2 = this.sortByKey(speakers, 'names');
    });
  }

  public sortByKey(array, key) {
    return array.sort(function (a, b) {
      var x = a[key]; var y = b[key];
      return ((x < y) ? -1 : ((x > y) ? 0 : 1));
    });
  }

  updateComment() {
    this.commentService.getComments().subscribe(async res => {
      this.comments_items = this.sortByKey(res.filter(item => item.eventid == this.session.id), 'date');

      this.eventsService.setCurrent(this.comments_items);
    });
  }

  openFile(url) {
    this.iab.create(url.file);
  }

  goToSessionDetail(sessionData: any) {
    this.router.navigate([`tabs/schedule`]);
  }


  private updateMapElement() {
    const mapData = {
      origin: {
        lat: -29.853935,
        lng: 31.026966,
        name: 'Durban ICC',
      },
      zoom: parseInt('18', 10),
    };

    const mapEle = this.mapElement.nativeElement;

    const map = new google.maps.Map(mapEle, {
      center: mapData.origin,
      zoom: mapData.zoom,
    });
    const marker = new google.maps.Marker({
      position: mapData.origin,
      map: map,
      title: mapData.origin.name,
    });
  }

  rate1: boolean = false;
  rate2: boolean = false;
  rate3: boolean =  false;
  rate4: boolean =  false;
  rate5: boolean =  false;


  share() {
    this.socialSharing.share(this.session.description + ', ' + this.session.location + ', ' + this.session.date, this.session.title,null, 'https://www.moseskotaneinstitute.com/').then(() => {
      // Sharing via email is possible
    }).catch(() => {
      // Sharing via email is not possible
    });
  }


  init() {
    this.activatedRoute.params.subscribe((params) => {
      this.updateMapElement();
      console.log('here');
      this.scheduleService.getScheduleDays(false, '', '', '')
        .then((days) => {
          this.session = (days as any[]).filter((day) => {
            return day.type === 'subsession';
          })
            .find((day) => {
              return day.subsession.id === params.id;
              
            });
        });
    });

  }

  isInFavorites(key: string) {
    if (key == 'yes') {
      return true;
    } else {
      return false;
    }
    //return this.eventsService.isInFavorites(key);
  }


  ngAfterViewInit(): void {
    if (this.session) {
      this.updateMapElement();
    }
  }

  async deleteevent(id) {
    this.eventsService.removeEvent(id);

    const toast = await this.toastCtrl.create({
      message: 'Event deleted',
      duration: 3000,
    });
    toast.present();

    this.router.navigate(['tabs/schedule']);
  }



  async addComment(form: NgForm, comment, filename) {

    if (form.valid) {

      const toast = await this.toastCtrl.create({
        message: 'Comment added successfully',
        duration: 3000,
      });
      toast.present();
      return new Promise<any>((resolve, reject) => {
        this.afs.collection('/comments').add({
          comment: this.comment.comment,
          date: new Date(),
          email: this.useremail,
          name: this.useremail,
          eventid: this.session.id,
          user: this.usernames,
        })
          .then(
            (res) => {
              resolve(res)
              this.comment.comment = '';
            },
            err => reject(err)
          )
      })


    }


  }


  async deletecomment(id) {
    this.commentService.removeComment(id);

    const toast = await this.toastCtrl.create({
      message: 'Comment deleted successfully',
      duration: 3000,
    });
    toast.present();
  }



  updateRating(tot, count) {

    var avgtotRate = tot / count;

    avgtotRate = Math.round(avgtotRate);

    if (avgtotRate >= 1) {
      this.rate1 = true;
    } else {
      this.rate1 = false;
    }
    if (avgtotRate >= 2) {
      this.rate2 = true;
    } else {
      this.rate2 = false;
    }
    if (avgtotRate >= 3) {
      this.rate3 = true;
    } else {
      this.rate3 = false;
    }
    if (avgtotRate >= 4) {
      this.rate4 = true;
    } else {
      this.rate4 = false;
    }
    if (avgtotRate >= 5) {
      this.rate5 = true;
    } else {
      this.rate5 = false;
    }
  }

  ratedId = '';

  async rateSession(rate, event) {

    const toast = await this.toastCtrl.create({
      message: 'Rated ' + rate + ' stars',
      duration: 3000,
    });
    toast.present();

    event.rates = event.rates + 1;

    event.ratings = event.ratings + rate;

    this.eventsService.updateEvent(event, event.id);
    this.updateRating(event.ratings, event.rates);
    this.session = event;
  }


  async addrsvp(event) {
    const toast = await this.toastCtrl.create({
      message: 'RSVPed',
      duration: 3000,
    });
    toast.present();

    event.rsvps = event.rsvps + this.rsvpuser + '~';

    this.eventsService.updateEvent(event, event.id);

    this.session.rsvpd = true;
  }

  async deletersvp(event) {
    const toast = await this.toastCtrl.create({
      message: 'RSVP Removed',
      duration: 3000,
    });
    toast.present();

    event.rsvps = event.rsvps.replace(this.rsvpuser + '~', '');

    this.eventsService.updateEvent(event, event.id);

    this.session.rsvpd = false;
  }

 
  gotoMenu(menu) {
    this.router.navigate(['tabs/'+menu+'']);
  }
}
