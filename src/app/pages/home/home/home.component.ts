import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common'
import { Router, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { ModalController, IonItemSliding, ToastController } from '@ionic/angular';
import * as _ from 'lodash';
import { HomeService } from '../home.service';
import { HomeFilterComponent } from '../home-filter/home-filter.component';
import { Day, Session, SubSession } from 'src/app/interfaces/sessions';
import { EventService, Event } from 'src/app/services/events.service';
import { UserDataService } from 'src/app/services/common/user-data.service';
import { Observable, Subscription } from 'rxjs';
import { SessionrateService } from 'src/app/services/sessionrate.service';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-schedule',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers: [DatePipe]
})
export class HomeComponent implements OnInit {
  loadingtext = true;
  queryText = '';
  segment: 'all' | 'yes' = 'all';
  days: Day[] = [];
  days2: Day[] = [];
  filterBySessionId: string;
  filterByDayId: string;
  rate1: boolean = false;
  rate2: boolean = false;
  rate3: boolean = false;
  rate4: boolean = false;
  rate5: boolean = false;
  rsvpd: boolean = false;

  events: Event[] = [];
  events2: Event[] = [];
  rsvpuser: string = '';
  constructor(
    private scheduleService: HomeService,
    private modalCtrl: ModalController,
    private router: Router,
    private route: ActivatedRoute,
    private toastCtrl: ToastController,
    private titleService: Title,
    private eventsService: EventService,
    public datepipe: DatePipe,
    private userData: UserDataService,
    private sessionrateService: SessionrateService,
    public afs: AngularFirestore,
    private storage: Storage,
  ) { }
  useremail: string = '';
  ngOnInit() {
    this.titleService.setTitle('Home');
    this.storage.get('user').then((data) => {  this.rsvpuser = data.name + ' ' + data.surname + ',  ' + data.position + ' from ' + data.institution });

    this.userData.getEmail().subscribe(async res => {
      this.useremail = res;

    });

    this.updateHome();
  }

  ionViewWillEnter() {
    this.updateHome();
  }
  public sortByKey(array, key) {
    return array.sort(function (a, b) {
      var x = a[key]; var y = b[key];
      return ((x < y) ? -1 : ((x > y) ? 0 : 1));
    });
  }



  ngOnDestroy() { 
  }

  updateHome() {
    this.eventsService.gethomeEvents().subscribe(async res => {
       
      this.events = res;
      this.loadingtext = false;
      
      for (var a = 0; a < this.events.length; a++) {
        this.updateRating(this.events[a], this.events[a].ratings, this.events[a].rates);

        if (this.events[a].rsvps.indexOf(this.rsvpuser) > -1) {
          this.events[a].rsvpd = true;
        } else {
          this.events[a].rsvpd = false;
        }
      }

      this.eventsService.setCurrent(res);

    });
  }

  limitText(text) {

    var str = text;
    if (text.length > 150) {
      str = text.substring(0, 150);
    }

    return str + ' ...';
  }
   

  goToSessionDetail(sessionData: any) {
    this.eventsService.setCurrent(sessionData);
    this.router.navigate([`${sessionData.id}`], { relativeTo: this.route });
  }



  updateRating(event, tot, count) { 
    var avgtotRate = tot / count;

    avgtotRate = Math.round(avgtotRate);

    if (avgtotRate >= 1) {
      this.rate1 = true;
      event.rate1 = true;
    } else {
      this.rate1 = false;
      event.rate1 = false;
    }
    if (avgtotRate >= 2) {
      this.rate2 = true;
      event.rate2 = true;
    } else {
      this.rate2 = false;
      event.rate2 = false;
    }
    if (avgtotRate >= 3) {
      this.rate3 = true;
      event.rate3 = true;
    } else {
      this.rate3 = false;
      event.rate3 = false;
    }
    if (avgtotRate >= 4) {
      this.rate4 = true;
      event.rate4 = true;
    } else {
      this.rate4 = false;
      event.rate4 = false;
    }
    if (avgtotRate >= 5) {
      this.rate5 = true;
      event.rate5 = true;
    } else {
      this.rate5 = false
      event.rate5 = false;
    }
  }

  ratedId = '';

  async rateSession(rate, event) {

    const toast = await this.toastCtrl.create({
      message: 'Rated ' + rate + ' stars',
      duration: 3000,
    });
    toast.present();

    event.rates = event.rates + 1;

    event.ratings = event.ratings + rate;

    this.eventsService.updateEvent(event, event.id);
  }


  async addrsvp(event) { 
    const toast = await this.toastCtrl.create({
      message: 'RSVPed',
      duration: 3000,
    });
    toast.present();

    event.rsvps = event.rsvps + this.rsvpuser + '~';

    this.eventsService.updateEvent(event, event.id);
  }

  async deletersvp(event) {
    const toast = await this.toastCtrl.create({
      message: 'RSVP Removed',
      duration: 3000,
    });
    toast.present();

    event.rsvps = event.rsvps.replace(this.rsvpuser + '~', '');

    this.eventsService.updateEvent(event, event.id);
  }


  async eventlocation(event) {
    const toast = await this.toastCtrl.create({
      message: event.location,
      duration: 3000,
    });
    toast.present(); 
  }

  gotoMenu(menu) {
    this.router.navigate(['tabs/' + menu + '']);
  }
 
}
