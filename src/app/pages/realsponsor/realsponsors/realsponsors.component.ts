import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { NavController } from '@ionic/angular';
import { UserDataService } from 'src/app/services/common/user-data.service';
import { UserOptions, UserOptions1 } from 'src/app/interfaces/user-options';
import { EventsService } from 'src/app/services/common/events.service';
import { RealsponsorService } from 'src/app/services/realsponsors.service';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable, Subscription } from 'rxjs';
import { ChatService } from 'src/app/services/chat.service';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { Storage } from '@ionic/storage';
import * as firebase from 'firebase';
import { IonItemSliding, ToastController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-realsponsors',
  templateUrl: './realsponsors.component.html',
  styleUrls: ['./realsponsors.component.scss'],
})
export class RealsponsorsComponent implements OnInit {

  chat: any[] = [];
  chat2: any[] = [];
  queryText = '';

  signup: UserOptions1 = { name: '', surname: '', phone: '', email: '', password: '', institution: '', position: '', facebook: '', twitter: '', webpage: '', description: '' };
  submitted = false;
  signupError: string;
  admin: boolean = false;
  constructor(
    private navCtrl: NavController,
    private userData: UserDataService,
    private titleService: Title,
    private eventsService: EventsService,
    public afs: AngularFirestore,
    private sponsorsService: RealsponsorService,
    private toastCtrl: ToastController,
    private storage: Storage,
    private route: ActivatedRoute,
    private router: Router,
  ) {

    this.storage.get('user').then(async (data) => {
      try {
        if (data.role == '1') {
          this.admin = true;
        }
      } catch (exc) { }
    });
  }

  ngOnInit() {
    this.titleService.setTitle('Admin Users');
    this.updateChat();
  }

  private chatsubscription: Subscription = new Subscription();

  ngOnDestroy() {
    this.chatsubscription.unsubscribe();
  }

  async deleteBanner(id) {
    this.sponsorsService.removeRealsponsor(id);
    const toast = await this.toastCtrl.create({
      message: 'Removed Successfully!',
      duration: 3000,
    });
    toast.present();
  }

  goToSessionDetail(sessionData: any) {
    this.sponsorsService.setCurrent(sessionData);
    this.router.navigate([`${sessionData.id}`], { relativeTo: this.route });
  }

  updateChat() {
    this.chatsubscription.unsubscribe();
    this.chatsubscription = this.sponsorsService.getRealsponsors().subscribe(async (chat: any[]) => {

      this.chat = chat;
      this.chat2 = chat; 
    });
    
  }


  onSignup(form: NgForm) {
    this.submitted = true;
    this.signupError = '';

    if (form.valid) {
      this.addUser(this.signup); 
    }
  }
  captureDataUrl: string = '';

  async addUser(signup) {

    const toast = await this.toastCtrl.create({
      message: 'Sponsor added successfully',
      duration: 9000,
    });
    toast.present();
    return new Promise<any>((resolve, reject) => {
      this.afs.collection('/realsponsors').add({
        name: signup.name,
        phone: signup.phone,
        email: signup.email,
        description: signup.description,
        facebook: signup.facebook,
        twitter: signup.twitter,
        webpage: signup.webpage,
        image: this.captureDataUrl,
      })
        .then(
          (res) => {
            resolve(res)
          },
          err => reject(err)
        )
    })

  }






  getPicture(sourceType) {
    const cameraOptions: CameraOptions = {
      quality: 50,
      destinationType: Camera.DestinationType.DATA_URL,
      encodingType: Camera.EncodingType.JPEG,
      mediaType: Camera.MediaType.PICTURE,
      sourceType: sourceType
    };

    Camera.getPicture(cameraOptions)
      .then((captureDataUrl) => {
        this.captureDataUrl = 'data:image/jpeg;base64,' + captureDataUrl;
      }, (err) => {
        console.log(err);
      });
  }


  async upload(signup) {
    if (this.captureDataUrl != '') {
      let storageRef = firebase.storage().ref();
      // Create a timestamp as filename
      const filename = Math.floor(Date.now() / 1000);

      const imageRef = storageRef.child(`images/${filename}.jpg`);
      imageRef.putString(this.captureDataUrl, firebase.storage.StringFormat.DATA_URL)
        .then((snapshot) => {
          this.captureDataUrl = 'https://firebasestorage.googleapis.com/v0/b/' + storageRef.bucket + '/o/' + `images%2F${filename}.jpg` + '?alt=media';
          this.onSignup(signup);
        });
    } else {
      this.onSignup(signup);
    }
  }
   
}
