import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { RealsponsorComponent } from './realsponsor/realsponsor.component';
import { RealsponsorsComponent } from './realsponsors/realsponsors.component';

const routes: Routes = [
  {
    path: '',
    component: RealsponsorsComponent,
  },
  {
    path: ':id',
    component: RealsponsorComponent,
  },
];

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
  ],
  declarations: [
    RealsponsorsComponent,
    RealsponsorComponent,
  ],
})
export class RealsponsorsModule { }
