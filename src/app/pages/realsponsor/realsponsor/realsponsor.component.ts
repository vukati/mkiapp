import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RealsponsorService } from 'src/app/services/realsponsors.service';
import { CallService } from 'src/app/services/common/call.service';
import { EmailService } from 'src/app/services/common/email.service';
import { InAppBrowserService } from 'src/app/services/common/in-app-browser.service';
import { IonItemSliding, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-realsponsor',
  templateUrl: './realsponsor.component.html',
  styleUrls: ['./realsponsor.component.scss'],
})
export class RealsponsorComponent implements OnInit {

  realsponsor: any;

  constructor(
    private realsponsorsService: RealsponsorService,
    private callService: CallService,
    private emailService: EmailService,
    private inBrowser: InAppBrowserService,
    private activatedRoute: ActivatedRoute,
    private toastCtrl: ToastController,
  ) { }

  ngOnInit() {
    this.realsponsor = this.realsponsorsService.getCurrent();
    if (!this.realsponsor) {
      this.init();
    }
  }

  init() {
    this.activatedRoute.params.subscribe((params) => {
      this.realsponsorsService.getRealsponsor(params.id).subscribe((realsponsor) => {
        this.realsponsor = realsponsor;
      });
    });
  }

  call(phone: string) {
    this.callService.call(phone);
  }

  sendEmail(email: string) {
    this.emailService.sendEmail(email);
  }

  openUrl(url: string) {
    this.inBrowser.open(url);
  }


  async removeSponsor(id) {
    this.realsponsorsService.removeRealsponsor(id);

    const toast = await this.toastCtrl.create({
      message: 'Sponsor deleted',
      duration: 3000,
    });
    toast.present();

  }
}
