import { Injectable } from '@angular/core';
import * as _ from 'lodash';
import { DataService } from 'src/app/services/database/data.service';
import { DATA_SERVICE, DataServiceType } from 'src/config';
import { SpeakerService } from 'src/app/services/speaker.service';

@Injectable({
  providedIn: 'root',
})
export class ChatsService {
  private currentChat: any;

  constructor(
    private dataService: DataService,
    private speakersService: SpeakerService,
  ) { }

  getChatById(id: string) {
    //return this.speakersService.getChats()
    //  .then((chats) => {
    //    if (DATA_SERVICE === DataServiceType.firebase) {
    //      return chats.find((chat) => {
    //        return chat.$key === id;
    //      });
    //    } else if (DATA_SERVICE === DataServiceType.http) {
    //      return _.get(chats, id);
    //    }
    //  });
  }

  setCurrent(chat: any) {
    this.currentChat = chat;
  }

  getCurrent() {
    return this.currentChat;
  }
}
