import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { DataService } from 'src/app/services/database/data.service';
import { ChatService } from 'src/app/services/chat.service';
import { UserDataService } from 'src/app/services/common/user-data.service';
import * as firebase from 'firebase';
import { Observable, Subscription  } from 'rxjs';
import { first, map } from 'rxjs/operators';

@Component({
  selector: 'app-chat-list',
  templateUrl: './chat-list.component.html',
  styleUrls: ['./chat-list.component.scss'],
})
export class ChatListComponent implements OnInit {

  queryText = '';
  chat: any[];
  chat2: any[];

  constructor(
    private dataService: DataService,
    private router: Router,
    private route: ActivatedRoute,
    private chatService: ChatService,
    private titleService: Title,
    private userData: UserDataService,
  ) {
    this.ngOnInit();
  }
  useremail: string = '';
  ngOnInit() {
    this.titleService.setTitle('Peers');
 
    this.userData.getEmail().subscribe(async res => {
      
      this.useremail = res;
      this.updateChat();
    });

  }

  goToChatDetail(speaker: any) {
    this.chatService.setCurrent(speaker);
    this.router.navigate([`${speaker.$key}`], {relativeTo: this.route});
  }

  public sortByKey(array, key) {
    return array.sort(function (a, b) {
      var x = a[key]; var y = b[key];
      return ((x < y) ? -1 : ((x > y) ? 0 : 1));
    });
  }
  private chatsubscription: Subscription = new Subscription();

  ngOnDestroy() {
    this.chatsubscription.unsubscribe();
  }

  updateChat() {
     this.chatsubscription.unsubscribe();
     this.chatsubscription = this.chatService.getChatsUserslist().subscribe(async (chat: any[]) => {
       // for (var a = 0; a < chat.length; a++) {
       // if (chat[a].image.length > 0) {
       //   const aaaa = await this.getImgFromServer(chat[a].image);
       //   chat[a].image = aaaa;
       // }

       //}

      chat = chat.filter(item => item.email !== this.useremail)

      this.chat = chat;
      this.chat2 = chat;

      //if (this.queryText) {
      //  this.chat = this.chat.filter(item => item.names.toLowerCase().indexOf(this.queryText.toLowerCase()) !== -1)
      //}
    });

    if (this.queryText) {
      this.chat = this.chat.filter(item => item.name.toLowerCase().indexOf(this.queryText.toLowerCase()) !== -1)
    } else {
      this.chat = this.chat2;
    }
  }

  async getImgFromServer(imgName: string) {
    let img;
    let dowvnloadIMG;
    var retVal = '';
    img = firebase.storage().ref("/pictures/" + imgName).getDownloadURL();
    let ref = firebase.storage().ref();
    const imgRef = ref.child("/pictures/" + imgName);
    const downloadURL = await imgRef.getDownloadURL()

    retVal = downloadURL;

    return retVal
  }


  goToSpeakerDetail(c: any) {
    this.chatService.setCurrent(c);
    this.router.navigate([`${c.email}`], { relativeTo: this.route });
  }
}


