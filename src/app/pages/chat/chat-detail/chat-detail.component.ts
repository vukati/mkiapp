import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ChatService } from 'src/app/services/chat.service';
import { EmailService } from 'src/app/services/common/email.service';
import { InAppBrowserService } from 'src/app/services/common/in-app-browser.service';
import { CallService } from 'src/app/services/common/call.service';
import { IonContent } from '@ionic/angular';
import { UserDataService } from 'src/app/services/common/user-data.service';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';

import * as firebase from 'firebase';
import { Observable, Subscription } from 'rxjs';
import { first, map } from 'rxjs/operators';

@Component({
  selector: 'app-chat-detail',
  templateUrl: './chat-detail.component.html',
  styleUrls: ['./chat-detail.component.scss'],
})
export class ChatDetailComponent implements OnInit {
  @ViewChild('IonContent', { static: false }) content: IonContent
  paramData: any;
   userName: any;
  user_input: string = "";
  User: string = "";
  toUser: string = "";
  start_typing: any;
  loader: boolean;
  chat: any;
  useremail: string = '';
  image: string = '';
  msgList: any = [];
  session: any;
  loadingtext = true;
  constructor(
    private chatsService: ChatService,
    private callService: CallService,
    private emailService: EmailService,
    private inBrowser: InAppBrowserService,
    private activatedRoute: ActivatedRoute,
    private userData: UserDataService,
    public afs: AngularFirestore,
    private router: Router,
  ) { 
    
  }

  ngOnInit() {
    this.session = this.chatsService.getCurrent();
    console.log(this.session.email)
    this.toUser = this.session.email;
    this.image = this.session.image;
    this.userData.getEmail().subscribe(async res => {
      this.useremail = res;
      this.User = res;
    });
    this.init();
    //if (!this.chat) {
    //  this.init();
    //}
  }
  public sortByKey(array, key) {
    return array.sort(function (a, b) {
      var x = a[key]; var y = b[key];
      return ((x < y) ? -1 : ((x > y) ? 0 : 1));
    });
  }

  init() {
    this.chatsService.getChats().subscribe(async res => {
      try {
        this.chat = res.filter(item => (item.touser == this.session.email && item.fromuser == this.useremail) || (item.touser == this.useremail && item.fromuser == this.session.email));
        this.chat = this.sortByKey(this.chat, 'time')

        this.msgList = this.chat;
        this.scrollDown();
        this.loadingtext = false;

        setTimeout(() => {
          this.scrollDown()
        }, 50);

      } catch (exc) {

        this.router.navigate(['tabs/chat']);
      }
      });

  }

  async getImgFromServer(imgName: string) {
    let img;
    let dowvnloadIMG;
    var retVal = '';
    img = firebase.storage().ref("/pictures/" + imgName).getDownloadURL();
    let ref = firebase.storage().ref();
    const imgRef = ref.child("/pictures/" + imgName);
    const downloadURL = await imgRef.getDownloadURL()

    retVal = downloadURL;

    return retVal
  }

  call(phone: string) {
    this.callService.call(phone);
  }

  sendEmail(email: string) {
    this.emailService.sendEmail(email);
  }

  openLinkedin(linkedIn: string) {
    this.inBrowser.open(linkedIn);
  }

  openUrl(url: string) {
    this.inBrowser.open(url);
  }


  sendMsg() {

    var today = new Date();
    var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var dateTime = date + ' ' + time;

    return new Promise<any>((resolve, reject) => {



      this.afs.collection('/chats').add({
        touser: this.session.email,
        fromuser: this.useremail,
        fromuserName: this.useremail,
        userAvatar: this.session.image,
        time: dateTime,
        message: this.user_input,
      })
        .then(
          (res) => {
            resolve(res)
            this.Put();
          },
          err => reject(err)
        )
    })



   
  }


  Put() {
    var today = new Date();
    var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var dateTime = date + ' ' + time;

    if (this.user_input !== '') {
      this.msgList.push({
        userId: this.toUser,
        userName: this.toUser,
        userAvatar: this.session.image,
        time: dateTime,
        message: this.user_input,
        id: this.msgList.length + 1
      })
      this.user_input = "";
      this.scrollDown() 

    }

    this.user_input = "";
  }

  senderSends() {
    this.loader = true;
    setTimeout(() => {
      this.msgList.push({
        userId: this.User,
        userName: this.User,
        userAvatar: "../../assets/chat/chat5.jpg",
        time: "12:01",
        message: "Sorry, didn't get what you said. Can you repeat that please"
      });
      this.loader = false;
      this.scrollDown()
    }, 2000)
    this.scrollDown()
  }
  scrollDown() {
    setTimeout(() => {
      this.content.scrollToBottom(50)
    }, 50);
  }

  userTyping(event: any) {
    console.log(event);
    this.start_typing = event.target.value;
    this.scrollDown()
  }
}
