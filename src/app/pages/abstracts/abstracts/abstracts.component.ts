import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalController, IonItemSliding, ToastController } from '@ionic/angular';
import { AbstractsService } from '../abstracts.service';
import { AbstractsFilterComponent } from '../abstracts-filter/abstracts-filter.component';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { NavController, LoadingController } from '@ionic/angular';
import { SocialService, Social } from 'src/app/services/social.service';
import { Observable, Subscription } from 'rxjs'
import { HttpClient  } from '@angular/common/http';
import { first, map } from 'rxjs/operators';

@Component({
  selector: 'app-abstracts',
  templateUrl: './abstracts.component.html',
  styleUrls: ['./abstracts.component.scss'],
  providers: [InAppBrowser]
})
export class AbstractsComponent implements OnInit {

  queryText = '';
  segment: string =  'Links';
  abstracts: any[] = [];
  abstractsfeeds: any[] = [];
  get isFilterDirty(): boolean {
    return this.types && this.dates && (this.types.some((x) => !x.isChecked) || this.dates.some((x) => !x.isChecked));
  }

  private types: any[];
  private dates: any[];


  isConnected: boolean = false;
  notConnected: boolean = true;

  status: any;

  //step
  step1: boolean = true;
  step2: boolean = false;
  step3: boolean = false;


  news: any;
  rightNow: any;
  then: any;

  filterDate: any;
  social: Social = { id: '', title: '', link: '', date: '', type: '' };


  constructor(
    public navCtrl: NavController, private http: HttpClient, private loadingCtrl: LoadingController,
    private iab: InAppBrowser,
    private abstractsService: AbstractsService,
    private modalCtrl: ModalController,
    private router: Router,
    private route: ActivatedRoute,
    private toastCtrl: ToastController,
    private titleService: Title,
    private socialService: SocialService,
  ) {
    this.socialService.updateSocial(this.social, '2C1g8pQ6ej4iM6a3yeTc');
  }



  ngOnInit() {
    this.titleService.setTitle('Social Media');
    this.updateList();
    this.socialService.updateSocial(this.social, '2C1g8pQ6ej4iM6a3yeTc');
  }

  ionViewWillEnter() {
    Promise.all([this.loadTypes(), this.loadDates()])
      .then(() => this.updateList());
  }


  updateList() {
    this.socialService.getSocials().subscribe(async res => {
      this.abstracts = res.filter(i => i.title.indexOf(this.queryText) > -1 && i.link != '' && i.type != 'hashtag');
      this.abstractsfeeds = res.filter(i => i.title.indexOf(this.queryText) > -1 && i.link != '' && i.type == 'hashtag');
    });
  }

  async presentFilter() {
    const modal = await this.modalCtrl.create({
      component: AbstractsFilterComponent,
      componentProps: {
        dates: this.dates,
        types: this.types,
      },
    });
    await modal.present();

    const { data } = await modal.onWillDismiss();
    if (data) {
      this.dates = data.dates;
      this.types = data.types;
      this.updateList();
    }
  }

  goToAbstractDetail(abstract: any) {
    this.abstractsService.setCurrent(abstract);
    this.router.navigate([`${abstract.$key}`], {relativeTo: this.route});
  }

  async addFavorite(slidingItem: IonItemSliding, id: string) {
    slidingItem.close();

    this.abstractsService.toggleFavorites(id);

    const toast = await this.toastCtrl.create({
      message: 'Abstract added to favorites',
      duration: 3000,
    });
    toast.present();

    this.updateList();
  }

  async removeFavorite(slidingItem: IonItemSliding, id: string) {
    this.abstractsService.toggleFavorites(id);
    slidingItem.close();

    const toast = await this.toastCtrl.create({
      message: 'Abstract removed from favorites',
      duration: 3000,
    });
    toast.present();

    this.updateList();
  }

  clearFilter() {
    this.types.forEach((x) => x.isChecked = true);
    this.dates.forEach((x) => x.isChecked = true);
    this.updateList();
  }

  isInFavorites(key: string) {
    return this.abstractsService.isInFavorites(key);
  }

  private loadTypes() {
    return this.abstractsService.getTypes().then((types) => this.types = types);
  }

  private loadDates() {
    return this.abstractsService.getDates().then((dates) => this.dates = dates);
  }

  openFile(url) {
    this.iab.create(url);
  }
}
