import { Injectable } from '@angular/core';
import { DataService } from 'src/app/services/database/data.service';

@Injectable({
  providedIn: 'root',
})
export class MapService {

  constructor(
    private dataService: DataService,
  ) { }

  getMapDetails(): Promise<any> {
    return this.dataService.getMapDetails()
      .then((result) => {
        const data: any = {};
        data.origin = {
          lat: -29.8277733,
          lng: 30.9542836,
          icon: 'assets/img/marker1.png',
          name: 'Moses Kotane Institute',
        };

        const markers = [];

        markers.push({
          name: 'Moses Kotane Institute',
          icon: 'assets/img/marker1.png',
          lat: -29.8277733,
          lng: 30.9542836,
        });

        markers.push(data.origin);

        data.markers = markers;
        data.zoom = parseInt('18', 10);
        return data;
      });
  }
}
