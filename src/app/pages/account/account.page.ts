import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { NavController } from '@ionic/angular';
import { UserDataService } from 'src/app/services/common/user-data.service';
import { PersonService, Person } from 'src/app/services/user.service';
import { EventsService } from 'src/app/services/common/events.service';
import { IonItemSliding, ToastController } from '@ionic/angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { NgForm } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import * as firebase from 'firebase';
 import { first, map } from 'rxjs/operators';

@Component({
  selector: 'app-account',
  templateUrl: './account.page.html',
  styleUrls: ['./account.page.scss'],
})
export class AccountPage implements OnInit, AfterViewInit {

  email: string;
  person: Person = { id: '', name: '', surname: '', phone: '', email: '', image: '' };
  captureDataUrl: string = '';
  signupError: string = '';
  constructor(
    private navCtrl: NavController,
    private userDataService: UserDataService,
    private userService: PersonService,
    private titleService: Title,
    private eventsService: EventsService,
    private toastCtrl: ToastController,
  ) { }

  ngOnInit() {
    this.titleService.setTitle('Account');
  }

  ngAfterViewInit() {
    this.getEmail();

  }
  private chatsubscription3: Subscription = new Subscription();

  ngOnDestroy() {
    this.chatsubscription3.unsubscribe();
  }
  getEmail() {
    this.userDataService.getEmail().pipe(
      first(),
    ).subscribe((email) => {
      this.email = email;
      this.chatsubscription3 = this.userService.getPersons().subscribe(async res => {
        this.person = res.filter(i => i.email == this.email)[0];
 
      });
    });
  }

  logout() {
    this.userDataService.signOut();
    this.navCtrl.navigateRoot('login');
    this.eventsService.userLoggedOut(false);
  }


  async update(person) {
    this.userService.updatePerson(person, person.id);

    const toast = await this.toastCtrl.create({
      message: 'Account updated',
      duration: 3000,
    });
    toast.present();
  }


  async getImgFromServer(imgName: string) {
    let img;
    let dowvnloadIMG;
    var retVal = '';
    img = firebase.storage().ref("/pictures/" + imgName).getDownloadURL();
    let ref = firebase.storage().ref();
    const imgRef = ref.child("/pictures/" + imgName);
    const downloadURL = await imgRef.getDownloadURL()

    retVal = downloadURL;

    return retVal
  }



  getPicture(sourceType) {
    const cameraOptions: CameraOptions = {
      quality: 50,
      destinationType: Camera.DestinationType.DATA_URL,
      encodingType: Camera.EncodingType.JPEG,
      mediaType: Camera.MediaType.PICTURE,
      sourceType: sourceType
    };

    Camera.getPicture(cameraOptions)
      .then((captureDataUrl) => {
        this.captureDataUrl = 'data:image/jpeg;base64,' + captureDataUrl;
      }, (err) => {
        console.log(err);
      });
  }



  async updateUser(form: NgForm, person, filename) {
 
    if (form.valid) {



      if (this.captureDataUrl == '') {
        this.userService.updatePerson(this.person, this.person.id);
      } else {
        let storageRef = firebase.storage().ref();
        // Create a timestamp as filename
        const filename = Math.floor(Date.now() / 1000);

        // Create a reference to 'images/todays-date.jpg'
        const imageRef = storageRef.child(`images/${filename}.jpg`);
        imageRef.putString(this.captureDataUrl, firebase.storage.StringFormat.DATA_URL)
          .then((snapshot) => {
            this.person.image = 'https://firebasestorage.googleapis.com/v0/b/' + storageRef.bucket + '/o/' + `images%2F${filename}.jpg` + '?alt=media';
            this.userService.updatePerson(this.person, this.person.id);
          });
      }

       
      const toast = await this.toastCtrl.create({
        message: 'Account updated',
        duration: 3000,
      });
      toast.present();
    }
  }
}
