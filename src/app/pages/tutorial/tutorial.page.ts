import { Component, OnInit, ViewChild } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { IonSlides, MenuController, NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { BannerOptions } from 'src/app/interfaces/banner-options';
import { BannerService, Banner } from 'src/app/services/banners.service';
import * as firebase from 'firebase';
import { Observable, Subscription } from 'rxjs';
import { first, map } from 'rxjs/operators';

@Component({
  selector: 'app-tutorial',
  templateUrl: './tutorial.page.html',
  styleUrls: ['./tutorial.page.scss'],
})
export class TutorialPage implements OnInit {

  @ViewChild('slides', { static: false }) slides: IonSlides;
  showSkip = true;

  banner: BannerOptions = { title: '', content: '', image: '' };
  captureDataUrl: string = '';
  loaded: boolean = false;
  banners_items: Banner[];
  constructor(
    private storage: Storage,
    private menu: MenuController,
    private navCtrl: NavController,
    private titleService: Title,
    private bannerService: BannerService,
  ) { }

  ngOnInit() {
    this.titleService.setTitle('MKI - Conference App');

    this.loadSlides();
  }

  private chatsubscription: Subscription = new Subscription();

  ngOnDestroy() {
    this.chatsubscription.unsubscribe();
  }

  loadSlides() {
    this.chatsubscription.unsubscribe();
    this.chatsubscription = this.bannerService.getBanners().subscribe(res => {
      this.banners_items = res;
      this.loaded = true;
    });
  }

  async getImgFromServer(imgName: string) {
    let img;
    let dowvnloadIMG;
    var retVal = '';
    img = firebase.storage().ref("/images/" + imgName).getDownloadURL();
    let ref = firebase.storage().ref();
    const imgRef = ref.child("/images/" + imgName);
    const downloadURL = await imgRef.getDownloadURL()

    retVal = downloadURL;

    return retVal
  }

  startApp() {
    this.navCtrl.navigateRoot('/login').then(() => {
      this.storage.set('hasSeenTutorial', 'true');
    });
  }

  onSlideChangeStart() {
    this.slides.isEnd().then((result) => {
      this.showSkip = !result;
    });
  }

  ionViewWillEnter() {
    this.slides.update();
    this.loadSlides();
  }

  ionViewDidEnter() {
    // the root left menu should be disabled on the tutorial page
    this.menu.enable(false);
  }

  ionViewDidLeave() {
    // enable the root left menu when leaving the tutorial page
    this.menu.enable(true);
  }
}
