import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PersonService } from 'src/app/services/user.service';
import { CommitteesService } from '../committees.service';
@Component({
  selector: 'app-members',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.scss'],
})
export class MembersComponent implements OnInit {

  committee: any;

  get members() {
    if (!this.committee.members) {
      return [];
    }
    return Object.keys(this.committee.members).map((id) => {
      return {...this.committee.members[id], $key: id};
    });
  }

  constructor(
    private committeesService1: PersonService,
    private committeesService: CommitteesService,
    private router: Router,
    private route: ActivatedRoute,
    private activatedRoute: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.committee = this.committeesService.getCurrent();
    if (!this.committee) {
      this.init();
    }

    console.log(this.committee.members)
  }

  init() {
    this.activatedRoute.params.subscribe((params) => {
      this.committeesService1.getUser1()
        .subscribe((committees) => {
          this.committee = committees.find((committiee) => {
            return committiee.id === params.id;
          });
        });
    });
  }

  goToMemberDetail(member: any) {
    this.committeesService.setCurrentMember(member);
    this.router.navigate(['member', `${member.id}`], {relativeTo: this.route});
  }
}
