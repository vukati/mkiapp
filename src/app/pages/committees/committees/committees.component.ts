import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { PersonService } from 'src/app/services/user.service';
import { CommitteesService } from '../committees.service';
@Component({
  selector: 'app-committees',
  templateUrl: './committees.component.html',
  styleUrls: ['./committees.component.scss'],
})


export class CommitteesComponent implements OnInit {

  queryText = '';
  members: members = { firstName: '', lastName: '', email: '', affiliation: '', avatar: '', phoneNumber: '', id: '' };
  committees: commitees[] = [];

  constructor(
    private committeesService: CommitteesService,
    private committeesService1: PersonService,
    private router: Router,
    private route: ActivatedRoute,
    private titleService: Title,
  ) { }

  ngOnInit() {
    this.titleService.setTitle('Attendees');
    this.updateList();
  }

  members1: members[] = [];
  members2: members[] = [];

  updateList() {
    this.committeesService1.getUser1()
      .subscribe((res) => {
        
        this.members1 = [];
        this.members2 = [];

        for (var a = 0; a < res.length; a++){
          
          this.members = { firstName: '', lastName: '', email: '', affiliation: '', avatar: '', phoneNumber: '', id:'' };
          if (res[a].role == '1') {
            this.members.affiliation = res[a].institution;
            this.members.firstName = res[a].name;
            this.members.lastName = res[a].surname;
            this.members.email = res[a].email;
            this.members.avatar = res[a].image;
            this.members.phoneNumber = res[a].phone;
            this.members.id = res[a].id;
            
            this.members1.push(this.members);
          } else {
            this.members.affiliation = res[a].institution;
            this.members.firstName = res[a].name;
            this.members.lastName = res[a].surname;
            this.members.email = res[a].email;
            this.members.avatar = res[a].image;
            this.members.phoneNumber = res[a].phone;

            this.members2.push(this.members);
          }
           
        }

        this.committees.push({ title: 'Organizers', members: this.members1 })
        this.committees.push({ title: 'Attendees', members: this.members2 }) 

      });
  }

  goToEventDetail(committee: any) {
    this.committeesService.setCurrent(committee);
    this.router.navigate([`${committee.title}`], {relativeTo: this.route});
  }
}




export interface commitees {
  title: string,
  members: members[],
}

export interface members {
  id: string,
  firstName: string,
  lastName: string,
  email: string,
  affiliation: string,
  avatar: string,
  phoneNumber: string,
}
