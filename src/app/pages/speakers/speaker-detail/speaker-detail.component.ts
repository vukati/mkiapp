import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SpeakerService } from 'src/app/services/speaker.service';
import { EmailService } from 'src/app/services/common/email.service';
import { InAppBrowserService } from 'src/app/services/common/in-app-browser.service';
import { CallService } from 'src/app/services/common/call.service';
import { UserDataService } from 'src/app/services/common/user-data.service';
import * as firebase from 'firebase';
import { first, map } from 'rxjs/operators';
import { SpeakerrateService } from 'src/app/services/speakerrate.service';
import { Observable, Subscription } from 'rxjs';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { IonItemSliding, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-speaker-detail',
  templateUrl: './speaker-detail.component.html',
  styleUrls: ['./speaker-detail.component.scss'],
})
export class SpeakerDetailComponent implements OnInit {

  speaker: any;

  rate1: boolean = false;
  rate2: boolean = false;
  rate3: boolean = false;
  rate4: boolean = false;
  rate5: boolean = false;

  constructor(
    private speakersService: SpeakerService,
    private callService: CallService,
    private emailService: EmailService,
    private inBrowser: InAppBrowserService,
    private activatedRoute: ActivatedRoute,
    private sessionrateService: SpeakerrateService,
    private userData: UserDataService,
    public afs: AngularFirestore,
    private toastCtrl: ToastController,
  ) { }
  useremail: string = '';
  ngOnInit() {
    this.speaker = this.speakersService.getCurrent();
    this.updateRating(this.speaker.ratings, this.speaker.rates);
    if (!this.speaker) {
      this.init();
    }


    this.userData.getEmail().subscribe(async res => {
      this.useremail = res;
    });

   }

  init() {
 
    

    this.activatedRoute.params.subscribe((params) => {
      this.speakersService.getSpeaker(params.id).subscribe(async res => {
        this.speaker = res;
      });
    });
  }

  private ratesubscription: Subscription = new Subscription();

  ngOnDestroy() {
    this.ratesubscription.unsubscribe();
  }



  updateRating(tot, count) {
    var avgtotRate = tot / count;

    avgtotRate = Math.round(avgtotRate);

    if (avgtotRate >= 1) {
      this.rate1 = true;
    } else {
      this.rate1 = false;
    }
    if (avgtotRate >= 2) {
      this.rate2 = true;
    } else {
      this.rate2 = false;
    }
    if (avgtotRate >= 3) {
      this.rate3 = true;
    } else {
      this.rate3 = false;
    }
    if (avgtotRate >= 4) {
      this.rate4 = true;
    } else {
      this.rate4 = false;
    }
    if (avgtotRate >= 5) {
      this.rate5 = true;
    } else {
      this.rate5 = false;
    }
  }

  ratedId = '';

  async rateSession(rate, speaker) {

    const toast = await this.toastCtrl.create({
      message: 'Rated ' + rate + ' stars',
      duration: 3000,
    });
    toast.present();

    speaker.rates = speaker.rates + 1;

    speaker.ratings = speaker.ratings + rate;

    this.speakersService.updateSpeaker(speaker, speaker.id);
  }

  call(phone: string) {
    this.callService.call(phone);
  }

  sendEmail(email: string) {
    this.emailService.sendEmail(email);
  }

  openLinkedin(linkedIn: string) {
    this.inBrowser.open(linkedIn);
  }

  openUrl(url: string) {
    this.inBrowser.open(url);
  }

}
