import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { DataService } from 'src/app/services/database/data.service';
import { SpeakerService, Speaker } from 'src/app/services/speaker.service';
import * as firebase from 'firebase';
import { Observable, Subscription } from 'rxjs';
import { first, map } from 'rxjs/operators';

@Component({
  selector: 'app-speakers-list',
  templateUrl: './speakers-list.component.html',
  styleUrls: ['./speakers-list.component.scss'],
})
export class SpeakersListComponent implements OnInit {

  queryText = '';
  speakers: any[] = [];
  speakers2: any[] = [];
  s: Speaker = { names: '', institution: '', biography: '', phone: '', email: '', linkedin: '', webpage: '', eventid: '', picture: '' }

  constructor(
    private dataService: DataService,
    private router: Router,
    private route: ActivatedRoute,
    private speakersService: SpeakerService,
    private titleService: Title,
  ) {
    this.s.names = Date.now().toString();
    this.speakersService.updateSpeaker(this.s, 'tKnIDk1qyDXGwWXSWmLj');
  }

  ngOnInit() {
    this.titleService.setTitle('Speakers');
    this.updateSpeakers();
  }

  goToSpeakerDetail(speaker: any) {
    this.speakersService.setCurrent(speaker);
    this.router.navigate([`${speaker.id}`], {relativeTo: this.route});
  }

  public sortByKey(array, key) {
    return array.sort(function (a, b) {
      var x = a[key]; var y = b[key];
      return ((x < y) ? -1 : ((x > y) ? 0 : 1));
    });
  }
  private chatsubscription: Subscription = new Subscription();

  ngOnDestroy() {
    this.chatsubscription.unsubscribe();
  }
  updateSpeakers() {
    this.chatsubscription.unsubscribe();
    this.chatsubscription = this.speakersService.getSpeakers().subscribe(async (speakers: any[]) => {
      speakers = speakers.filter(i => i.eventid != '')
      //for (var a = 0; a < speakers.length; a++) {
      //  if (speakers[a].picture.length > 0) {
      //    const aaaa = await this.getImgFromServer(speakers[a].picture);
      //    speakers[a].picture = aaaa;
      //  }

      //}

      this.speakers = this.sortByKey(speakers, 'names');
      this.speakers2 = this.sortByKey(speakers, 'names');

      if (this.queryText) {
        this.speakers = this.speakers.filter(item => item.names.toLowerCase().indexOf(this.queryText.toLowerCase()) !== -1)
      }
    });

    if (this.queryText) {
      this.speakers = this.speakers.filter(item => item.names.toLowerCase().indexOf(this.queryText.toLowerCase()) !== -1)
    } else {
      this.speakers = this.speakers2;
    }
  }

  async getImgFromServer(imgName: string) {
    let img;
    let dowvnloadIMG;
    var retVal = '';
    img = firebase.storage().ref("/pictures/" + imgName).getDownloadURL();
    let ref = firebase.storage().ref();
    const imgRef = ref.child("/pictures/" + imgName);
    const downloadURL = await imgRef.getDownloadURL()

    retVal = downloadURL;

    return retVal
  }
}


