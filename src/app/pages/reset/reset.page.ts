import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { NavController } from '@ionic/angular';
import { UserOptions } from 'src/app/interfaces/user-options';
import { UserDataService } from 'src/app/services/common/user-data.service';
import { EventsService } from 'src/app/services/common/events.service';

@Component({
  selector: 'app-reset',
  templateUrl: './reset.page.html',
  styleUrls: ['./reset.page.scss'],
})
export class ResetPage implements OnInit {

  login: UserOptions = { name: '', surname: '', phone: '', email: '', password: '', institution: '', position: '' };
  submitted = false;
  loginError: string;

  constructor(
    private userData: UserDataService,
    private navCtrl: NavController,
    private router: Router,
    private titleService: Title,
    private eventsService: EventsService,
  ) { }

  ngOnInit() {
    this.titleService.setTitle('Reset Password');
  }

  onLogin(form: NgForm) {
    this.loginError = '';
    this.submitted = true;

    if (form.valid) { 

      this.userData.resetPassword(this.login.email).then(
        () => {
          this.loginError = "Email was successfully sent to reset your password.";
        },
        (error) => this.loginError = error.message,
      );
    }
  }

  onSignup() {
    this.router.navigate(['login']);
  }
}
