import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ExhibitService } from 'src/app/services/exhibits.service';
import { CallService } from 'src/app/services/common/call.service';
import { EmailService } from 'src/app/services/common/email.service';
import { InAppBrowserService } from 'src/app/services/common/in-app-browser.service';
import { IonItemSliding, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-exhibit',
  templateUrl: './exhibit.component.html',
  styleUrls: ['./exhibit.component.scss'],
})
export class ExhibitComponent implements OnInit {

  exhibit: any;

  constructor(
    private exhibitsService: ExhibitService,
    private callService: CallService,
    private emailService: EmailService,
    private inBrowser: InAppBrowserService,
    private activatedRoute: ActivatedRoute,
    private toastCtrl: ToastController,
  ) { }

  ngOnInit() {
    this.exhibit = this.exhibitsService.getCurrent();
    if (!this.exhibit) {
      this.init();
    }
  }

  init() {
    this.activatedRoute.params.subscribe((params) => {
      this.exhibitsService.getExhibit(params.id).subscribe((exhibit) => {
        this.exhibit = exhibit;
      });
    });
  }

  call(phone: string) {
    this.callService.call(phone);
  }

  sendEmail(email: string) {
    this.emailService.sendEmail(email);
  }

  openUrl(url: string) {
    this.inBrowser.open(url);
  }


  async removeSponsor(id) {
    this.exhibitsService.removeExhibit(id);

    const toast = await this.toastCtrl.create({
      message: 'Sponsor deleted',
      duration: 3000,
    });
    toast.present();

  }
}
