import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { ExhibitComponent } from './exhibit/exhibit.component';
import { ExhibitsComponent } from './exhibits/exhibits.component';

const routes: Routes = [
  {
    path: '',
    component: ExhibitsComponent,
  },
  {
    path: ':id',
    component: ExhibitComponent,
  },
];

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
  ],
  declarations: [
    ExhibitsComponent,
    ExhibitComponent,
  ],
})
export class ExhibitsModule { }
