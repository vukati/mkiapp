import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common'
import { Router, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { ModalController, IonItemSliding, ToastController } from '@ionic/angular';
import * as _ from 'lodash';
import { ScheduleService } from '../schedule.service';
import { ScheduleFilterComponent } from '../schedule-filter/schedule-filter.component';
import { Day, Session, SubSession } from 'src/app/interfaces/sessions';
import { EventService, Event } from 'src/app/services/events.service';
import { UserDataService } from 'src/app/services/common/user-data.service';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.scss'],
  providers: [DatePipe]
})
export class ScheduleComponent implements OnInit {

  queryText = '';
  segment: 'all' | 'yes' = 'all';
  days: Day[] = [];
  days2: Day[] = [];
  days_: Day[] = [];
  days2_: Day[] = [];
  filterBySessionId: string;
  filterByDayId: string;

  constructor(
    private scheduleService: ScheduleService,
    private modalCtrl: ModalController,
    private router: Router,
    private route: ActivatedRoute,
    private toastCtrl: ToastController,
    private titleService: Title,
    private eventsService: EventService,
    public datepipe: DatePipe,
    private userData: UserDataService,
  ) { }
  useremail: string = '';
  ngOnInit() {
    this.titleService.setTitle('Schedule');

    this.userData.getEmail().subscribe(async res => {
      this.useremail = res;

    });

    this.updateSchedule();
  }

  ionViewWillEnter() {
    this.updateSchedule();
  }
  public sortByKey(array, key) {
    return array.sort(function (a, b) {
      var x = a[key]; var y = b[key];
      return ((x < y) ? -1 : ((x > y) ? 0 : 1));
    });
  }
  private chatsubscription1: Subscription = new Subscription();

  ngOnDestroy() {
    this.chatsubscription1.unsubscribe();
  }
  updateSchedule() {

    this.chatsubscription1.unsubscribe();
    this.chatsubscription1 = this.eventsService.getEvents().subscribe(async res => {

      this.days = this.convertScheduleDays(res.filter(item => item.owner == '' || item.owner == this.useremail));
      this.days2 = this.convertScheduleDays(res.filter(item => item.owner == '' || item.owner == this.useremail));


      if (this.segment == 'yes') {
        this.days = this.days.filter(item => item.sessions = item.sessions.filter(item => item.favorite === this.segment));
      } else {
        this.days = this.days2;
      }

      this.days = this.days.filter(item => item.sessions = this.sortByKey(item.sessions, 'time'))
      this.days2 = this.days.filter(item => item.sessions = this.sortByKey(item.sessions, 'time'))

      this.days = this.sortByKey(this.days, 'date');
      this.days2 = this.sortByKey(this.days, 'date');

    });

    
    this.eventsService.getSessions().subscribe(async result_ => {

      const result: Day[] = [];

      for (const res of result_) {
        const newDay: Day = {
          date: res.date,
          sessions: [],
          $key: res.id,
        };

        const newSession: Session = {
          time: res.startdatetime,
          subSessions: [],
          $key: res.id,
          favorite: res.favorite,
        };


        const newSubSession: SubSession = {
          date: res.date,
          subsession: { $key: res.id, timeStart: res.startdatetime, timeEnd: res.enddatetime, location: res.location, title: res.title, description: res.description, owner: res.owner, favorite: res.favorite, speakers: [] },
        };

        newSession.subSessions.push(newSubSession);
        newDay.sessions.push(newSession);

        result.push(newDay);
      }

      this.days_ = result;
      this.days2_ = result;

      if (this.segment == 'yes') {
        this.days_ = this.days_.filter(item => item.sessions = item.sessions.filter(item => item.favorite === this.segment));
      } else {
        this.days_ = this.days2_;
      }

      this.days_ = this.days_.filter(item => item.sessions = this.sortByKey(item.sessions, 'time'))
      this.days2_ = this.days_.filter(item => item.sessions = this.sortByKey(item.sessions, 'time'))

      this.days_ = this.sortByKey(this.days_, 'date');
      this.days2_ = this.sortByKey(this.days_, 'date');
    });



    try {

      if (typeof (this.filterByDayId) == 'undefined') {

      } else if (this.filterByDayId == null) {
        this.days = this.days2;
      } else {
        this.days = this.days.filter(item => item.$key === this.filterByDayId);




        try {

          if (typeof (this.filterBySessionId) == 'undefined') {

          } else if (this.filterBySessionId == null) {

          } else {
            this.days = this.days.filter(item => item.sessions = item.sessions.filter(item => item.$key === this.filterBySessionId));
          }


        } catch (etc) {

        }

      }

      if (this.segment == 'yes') {
        this.days = this.days.filter(item => item.sessions = item.sessions.filter(item => item.favorite === this.segment));
      } else {
        this.days = this.days2;
      }

    } catch (etc) {
      this.days = this.days2;

      if (this.segment == 'yes') {
        this.days = this.days.filter(item => item.sessions = item.sessions.filter(item => item.favorite === this.segment));
      } else {
        this.days = this.days2;
      }

    }

  }

  async presentFilter() {
    const days = this.days;
    const modal = await this.modalCtrl.create({
      component: ScheduleFilterComponent,
      componentProps: {
        days: days,
        filterBySessionId: this.filterBySessionId,
        filterByDayId: this.filterByDayId,
      },
    });
    await modal.present();
    const { data } = await modal.onWillDismiss();

    if (data) {
      this.filterBySessionId = data.filterBySessionId === 'all' ? null : data.filterBySessionId;
      this.filterByDayId = data.filterByDayId === 'all' ? null : data.filterByDayId;
      this.updateSchedule();
    }
  }

  goToSessionDetail(sessionData: any) {
    this.scheduleService.setCurrent(sessionData);
    this.router.navigate([`${sessionData.subsession.$key}`], { relativeTo: this.route });
  }

  async addFavorite(slidingItem: IonItemSliding, sessionData: any) {
    slidingItem.close();

    if (!this.scheduleService.isInFavorites(sessionData.$key)) {
      this.scheduleService.toggleFavorites(sessionData.$key);

      const toast = await this.toastCtrl.create({
        message: 'Session added to favorites',
        duration: 3000,
      });
      toast.present();

      this.updateSchedule();
    } else {
      const toast = await this.toastCtrl.create({
        message: 'Session is already in favorites',
        duration: 3000,
      });
      toast.present();
    }
  }

  async removeFavorite(slidingItem: IonItemSliding, sessionData: any) {
    this.scheduleService.toggleFavorites(sessionData.$key);
    slidingItem.close();

    const toast = await this.toastCtrl.create({
      message: 'Session removed from favorites',
      duration: 3000,
    });
    toast.present();

    this.updateSchedule();
  }

  clearFilter() {
    this.filterByDayId = null;
    this.filterBySessionId = null;
    this.updateSchedule();
  }

  private convertScheduleDays(dayss): Day[] {
    const result: Day[] = [];
    for (const days of dayss) {

      var dys = [];
      for (const day of days.sessions) {
        dys.push(day.date);
      }

      var flags = [], output = [], l = days.length, i;
      for (i = 0; i < l; i++) {
        if (flags[this.datepipe.transform(days[i].date, 'yyyy-MM-dd')]) continue;
        flags[this.datepipe.transform(days[i].date, 'yyyy-MM-dd')] = true;
        output.push(this.datepipe.transform(days[i].date, 'yyyy-MM-dd'));
      }
      for (const day of days.sessions) {

        if (result.length > 0) {

          try {
            result.find(item => this.datepipe.transform(item.date, 'yyyy-MM-dd') === this.datepipe.transform(day.date, 'yyyy-MM-dd')).date;

            const newSession: Session = {
              time: day.startdatetime,
              subSessions: [],
              $key: day.id,
              favorite: day.favorite,
            };


            const newSubSession: SubSession = {
              date: day.date,
              subsession: { $key: days.id, timeStart: day.startdatetime, timeEnd: day.enddatetime, location: days.location, title: day.title, description: day.description, owner: days.owner, favorite: day.favorite, speakers: [] },
            };

            newSession.subSessions.push(newSubSession);
            try {
              result.find(item => this.datepipe.transform(item.date, 'yyyy-MM-dd') === this.datepipe.transform(day.date, 'yyyy-MM-dd')).sessions.push(newSession);

            } catch (ex) { }

          } catch (exc) {
            const newDay: Day = {
              date: day.date,
              sessions: [],
              $key: day.id,
            };

            const newSession: Session = {
              time: day.startdatetime,
              subSessions: [],
              $key: day.id,
              favorite: day.favorite,
            };


            const newSubSession: SubSession = {
              date: day.date,
              subsession: { $key: days.id, timeStart: day.startdatetime, timeEnd: day.enddatetime, location: days.location, title: day.title, description: day.description, owner: days.owner, favorite: day.favorite, speakers: [] },
            };

            newSession.subSessions.push(newSubSession);
            newDay.sessions.push(newSession);

            result.push(newDay);
          }

        } else {

          const newDay: Day = {
            date: day.date,
            sessions: [],
            $key: day.id,
          };

          const newSession: Session = {
            time: day.startdatetime,
            subSessions: [],
            $key: day.id,
            favorite: day.favorite,
          };


          const newSubSession: SubSession = {
            date: day.date,
            subsession: { $key: days.id, timeStart: day.startdatetime, timeEnd: day.enddatetime, location: days.location, title: day.title, description: day.description, owner: days.owner, favorite: day.favorite, speakers: [] },
          };

          newSession.subSessions.push(newSubSession);
          newDay.sessions.push(newSession);


          result.push(newDay);

        }

      }
    }


    return result;
  }

  addevent() {
    this.router.navigate(['tabs/addschedule']);
  }
}
