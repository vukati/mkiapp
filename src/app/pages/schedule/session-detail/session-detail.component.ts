import { ScheduleService } from '../schedule.service';
import { EventService } from 'src/app/services/events.service';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { NavController } from '@ionic/angular';
import { UserDataService } from 'src/app/services/common/user-data.service';
import { UserOptions, EventOptions, CommentOptions } from 'src/app/interfaces/user-options';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { IonItemSliding, ToastController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { CommentService, Comment } from 'src/app/services/comments.service'
import { SpeakerService } from 'src/app/services/speaker.service';
import { SessionrateService } from 'src/app/services/sessionrate.service';
import { Observable, Subscription } from 'rxjs';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

@Component({
  selector: 'app-session-detail',
  templateUrl: './session-detail.component.html',
  styleUrls: ['./session-detail.component.scss'],
  providers: [SocialSharing]
 })
export class SessionDetailComponent implements OnInit {
  session: any;
  useremail: string = '';

  comment: CommentOptions = { id: '', name: '', comment: '', email: '', date: '', eventid: '' };
  submitted = false;
  signupError: string;
  speakers: any[] = [];
  speakers2: any[] = [];
  constructor(
    private scheduleService: ScheduleService,
    private activatedRoute: ActivatedRoute,
    private commentService: CommentService,
    private navCtrl: NavController,
    private userData: UserDataService,
    private titleService: Title,
    private eventsService: EventService,
    public afs: AngularFirestore,
    private toastCtrl: ToastController,
    private router: Router,
    private speakersService: SpeakerService,
    private sessionrateService: SessionrateService,
    private socialSharing: SocialSharing
  ) { }
  comments_items: Comment[];
  ngOnInit() {
    this.session = this.scheduleService.getCurrent();
    console.log(this.session)
    if (!this.session) {
      this.init();
    }

    this.userData.getEmail().subscribe(async res => {
      this.useremail = res;

    });
    this.updateComment();
    this.updateRating();

    //this.speakersService.getSpeakers().subscribe((speakers: any[]) => {
    //  speakers = speakers.filter(i => i.eventid == this.session.subsession.$key)
    //  this.speakers = this.sortByKey(speakers, 'names');
    //  this.speakers2 = this.sortByKey(speakers, 'names');
    //});
  }

  public sortByKey(array, key) {
    return array.sort(function (a, b) {
      var x = a[key]; var y = b[key];
      return ((x < y) ? -1 : ((x > y) ? 0 : 1));
    });
  }
  private chatsubscription: Subscription = new Subscription();
  private ratesubscription: Subscription = new Subscription();

  ngOnDestroy() {
    this.chatsubscription.unsubscribe();
    this.ratesubscription.unsubscribe();
  }
  updateComment() {
    this.chatsubscription.unsubscribe();
    this.chatsubscription = this.commentService.getComments().subscribe(async res => {
      this.comments_items = res.filter(item => item.eventid == this.session.subsession.$key + this.session.subsession.title + this.session.subsession.timeStart);
    });
  }

  rate1: boolean = false;
  rate2: boolean = false;
  rate3: boolean =  false;
  rate4: boolean =  false;
  rate5: boolean =  false;

  updateRating() {
    this.ratesubscription.unsubscribe();
    this.ratesubscription = this.sessionrateService.getSessionrates().subscribe(async res => {
      res = res.filter(i => i.sessionid == this.session.subsession.$key + this.session.subsession.title + this.session.subsession.timeStart);
      var totRate = 0;
      var avgtotRate = 0;
      for (var a = 0; a < res.length; a++) {
        totRate = totRate + res[a].rate;
      }

      avgtotRate = totRate / res.length;

      avgtotRate = Math.round(avgtotRate);

      console.log(avgtotRate)

      if (avgtotRate >= 1) {
        this.rate1 = true;
      }
      if (avgtotRate >= 2) {
        this.rate2 = true;
      }
      if (avgtotRate >= 3) {
        this.rate3 = true;
      }
      if (avgtotRate >= 4) {
        this.rate4 = true;
      }
      if (avgtotRate >= 5) {
        this.rate5 = true;
      }

      
    });
  }

  share() {
    this.socialSharing.share(this.session.subsession.description + ', ' + this.session.subsession.location + ', ' + this.session.subsession.date, this.session.subsession.title,null, 'https://www.moseskotaneinstitute.com/').then(() => {
      // Sharing via email is possible
    }).catch(() => {
      // Sharing via email is not possible
    });
  }


  init() {
    this.activatedRoute.params.subscribe((params) => {
      this.scheduleService.getScheduleDays(false, '', '', '')
        .then((days) => {
          this.session = (days as any[]).filter((day) => {
            return day.type === 'subsession';
          })
            .find((day) => {
              return day.subsession.$key === params.id;
            });
        });
    });
  }

  isInFavorites(key: string) {
    if (key == 'yes') {
      return true;
    } else {
      return false;
    }
    //return this.eventsService.isInFavorites(key);
  }
  private chatsubscription3: Subscription = new Subscription();
  toggleFavorites(key) {
    this.chatsubscription3 = this.eventsService.getEvent(key).subscribe(i => {
      let event = i;
      if (event.favorite == 'yes') {
        event.favorite = 'no';
        this.session.subsession.favorite = 'no';
      } else {
        event.favorite = 'yes';
        this.session.subsession.favorite = 'yes';
      }
      console.log(this.session);
      this.eventsService.updateEvent(event, key);
      this.chatsubscription3.unsubscribe();
    });
  }

  async removeSession(id) {
    this.eventsService.removeSession(id);

    const toast = await this.toastCtrl.create({
      message: 'Session deleted',
      duration: 3000,
    });
    toast.present();

    this.router.navigate(['tabs/schedule']);
  }



  async addComment(form: NgForm, comment, filename) {

    if (form.valid) {

      const toast = await this.toastCtrl.create({
        message: 'Comment added successfully',
        duration: 3000,
      });
      toast.present();
      this.updateComment();
      return new Promise<any>((resolve, reject) => {
        this.afs.collection('/comments').add({
          comment: this.comment.comment,
          date: new Date(),
          email: this.useremail,
          name: this.useremail,
          eventid: this.session.subsession.$key + this.session.subsession.title + this.session.subsession.timeStart,
        })
          .then(
            (res) => {
              resolve(res)
            },
            err => reject(err)
          )
      })


    }


  }


  async deletecomment(id) {
    this.commentService.removeComment(id);

    const toast = await this.toastCtrl.create({
      message: 'Comment deleted successfully',
      duration: 3000,
    });
    toast.present();
  }

  async rateSession(rate, session) {


    const toast = await this.toastCtrl.create({
      message: 'Session rated ' + rate + ' stars',
      duration: 3000,
    });
    toast.present();


    return new Promise<any>((resolve, reject) => {
      this.afs.collection('/sessionrates').add({
        sessionid: this.session.subsession.$key + this.session.subsession.title + this.session.subsession.timeStart,
        rate: rate,
        rater: this.useremail,
      })
        .then(
          (res) => {
            resolve(res)
          },
          err => reject(err)
        )
    })
  }






  async rsvp(rate) {


    const toast = await this.toastCtrl.create({
      message: 'RSVPed',
      duration: 3000,
    });
    toast.present();


    return new Promise<any>((resolve, reject) => {
      this.afs.collection('/rsvps').add({
        sessionid: this.session.subsession.$key,
        user: this.useremail,
      })
        .then(
          (res) => {
            resolve(res)
          },
          err => reject(err)
        )
    })
  }

}
