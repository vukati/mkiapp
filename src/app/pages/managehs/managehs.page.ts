import { Component, OnInit, Input } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { NavController, AlertController } from '@ionic/angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { UserDataService } from 'src/app/services/common/user-data.service';
import { BannerOptions } from 'src/app/interfaces/banner-options';
import { EventsService } from 'src/app/services/common/events.service';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { BannerService, Banner } from 'src/app/services/banners.service';
import * as firebase from 'firebase';
import { Observable, Subscription } from 'rxjs';
import { first, map } from 'rxjs/operators';
import { IonItemSliding, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-managehs',
  templateUrl: './managehs.page.html',
  styleUrls: ['./managehs.page.scss'],
})
export class ManagehsPage implements OnInit {

  banner: BannerOptions = { title: '', content: '', image: '' };
  submitted = false;
  signupError: string;

  captureDataUrl: string = '';

  banners_items: Banner[];

  @Input('useURI') useURI: Boolean = true;

  constructor(
    private navCtrl: NavController,
    private userData: UserDataService,
    private titleService: Title,
    private eventsService: EventsService,
    public afs: AngularFirestore,
    private alertCtrl: AlertController,
    private bannerService: BannerService,
    private toastCtrl: ToastController,
  ) {
    this.alertCtrl = alertCtrl;
  }
  firestore = firebase.storage();
  private chatsubscription: Subscription = new Subscription();

  ngOnDestroy() {
    this.chatsubscription.unsubscribe();
  }
  ngOnInit() {
    this.titleService.setTitle('Manage Home Screen');

    this.chatsubscription.unsubscribe();
    this.chatsubscription = this.bannerService.getBanners1().subscribe(async res => {
      this.banners_items = res;
    });
     
  }


  getAllIomage(image) {
    var img;
    new Promise((resolve, reject) => {
      this.firestore.ref('/images/').child(image).getDownloadURL().then((url) => {
        img = url;
      })
    })

    return img;
  }

  getPicture(sourceType) {
    const cameraOptions: CameraOptions = {
      quality: 50,
      destinationType: Camera.DestinationType.DATA_URL,
      encodingType: Camera.EncodingType.JPEG,
      mediaType: Camera.MediaType.PICTURE,
      sourceType: sourceType
    };

    Camera.getPicture(cameraOptions)
      .then((captureDataUrl) => {
        this.captureDataUrl = 'data:image/jpeg;base64,' + captureDataUrl;
      }, (err) => {
        console.log(err);
      });
  }  


  async upload() {
    let storageRef = firebase.storage().ref();
    // Create a timestamp as filename
    const filename = Math.floor(Date.now() / 1000);

    const imageRef = storageRef.child(`images/${filename}.jpg`);
    imageRef.putString(this.captureDataUrl, firebase.storage.StringFormat.DATA_URL)
      .then((snapshot) => {
        this.addBanner(this.banner, 'https://firebasestorage.googleapis.com/v0/b/' + storageRef.bucket + '/o/' + `images%2F${filename}.jpg` + '?alt=media');
      });  
  }

  async getImgFromServer(imgName: string) {
    let img;
    let dowvnloadIMG;
    var retVal = '';
    img = firebase.storage().ref("/images/" + imgName).getDownloadURL();
    let ref = firebase.storage().ref();
    const imgRef = ref.child("/images/" + imgName);
    const downloadURL = await imgRef.getDownloadURL()

    retVal = downloadURL;

    return retVal
  }
  
  async addBanner(banner, filename) {
    this.signupError = '';
 
    if (this.banner.title == '' || this.banner.content == '') {
      this.signupError = 'All fields are required!';
    } else {
      return new Promise<any>((resolve, reject) => {
        this.afs.collection('/banners').add({
          title: this.banner.title,
          content: this.banner.content,
          image: filename,
        })
          .then(
            (res) => {
              resolve(res)
              this.banner.title = '';
              this.banner.content = '';
            },
            err => reject(err)
          )
      })
      
    }

    const toast = await this.toastCtrl.create({
      message: 'Content added to the home screen Successfully!',
      duration: 3000,
    });
    toast.present();
  }


  async deleteBanner(id) {
    this.bannerService.removeBanner(id);
    const toast = await this.toastCtrl.create({
      message: 'Banner removed Successfully!',
      duration: 3000,
    });
    toast.present();
  }
}
