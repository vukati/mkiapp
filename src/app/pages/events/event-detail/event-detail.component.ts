import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EventService, EventSession } from 'src/app/services/events.service';
import { IonItemSliding, ToastController } from '@ionic/angular';
import { SpeakerService, Speaker } from 'src/app/services/speaker.service';
import { Rsvp, RsvpService } from 'src/app/services/rsvp.service';
import { SocialService, Social } from 'src/app/services/social.service';
import { Observable, Subscription } from 'rxjs'
import { SpeakerOptions } from 'src/app/interfaces/user-options';
import { NgForm } from '@angular/forms';
import * as firebase from 'firebase';
import { first, map } from 'rxjs/operators';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { File, FileEntry } from '@ionic-native/file/ngx';
import { EventDataService } from 'src/app/event-data.service';
import { AngularFireStorage } from '@angular/fire/storage';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { FileChooser } from '@ionic-native/file-chooser/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
declare var google: any;
declare var cordova: any;

@Component({
  selector: 'app-event-detail',
  templateUrl: './event-detail.component.html',
  styleUrls: ['./event-detail.component.scss'],
  providers: [FileChooser, FilePath, AngularFireStorage, EventDataService, FileTransfer]
})
export class EventDetailComponent implements OnInit, AfterViewInit {
  session: any;
  @ViewChild('mapCanvas', {static: false}) mapElement: ElementRef;
  event: any;
  speaker: SpeakerOptions = { names: '', institution: '', biography: '', phone: '', email: '', linkedin: '', webpage: '', eventid: '', picture: '' };
  eventsession: EventSession = { title: '', description: '', date: '', startdatetime: '', enddatetime: '', ratings: 0, rate: 0, rates: 0 }
  social: Social = { id: '', title: '', link: '', date: '', type: ''};
  submitted = false;
  signupError: string;
  segment: string = 'Details';
  captureDataUrl: string = '';

  speakers_items: Speaker[];
  social_items: Social[];
  sessions_items: EventSession[] = [];
  rsvps_items: string[] = [];
  s: Speaker = { names: '', institution: '', biography: '', phone: '', email: '', linkedin: '', webpage: '', eventid: '', picture: '' }

  constructor(
    private eventsService: EventService,
    private activatedRoute: ActivatedRoute,
    private toastCtrl: ToastController,
    private router: Router,
    private speakerService: SpeakerService,
    private rsvpService: RsvpService,
    private socialService: SocialService,
    public afs: AngularFirestore,
    private transfer: FileTransfer,
    private eventsdata: EventDataService,
    private storage: AngularFireStorage,
    private file: File,
    private FileChooser: FileChooser,
    private FilePath: FilePath,
    private iab: InAppBrowser
  ) {
    this.s.names = Date.now().toString();
    this.socialService.updateSocial(this.social, '2C1g8pQ6ej4iM6a3yeTc');
    this.session = this.eventsService.getCurrent();
    this.sessions_items = this.session.sessions;
    this.rsvps_items = this.session.rsvps.split('~');
    this.speakerService.updateSpeaker(this.s, 'tKnIDk1qyDXGwWXSWmLj');


    this.files = this.session.documents;
    this.speakerService.getSpeakers().subscribe(async res => {
      res = res.filter(i => i.eventid != '')
      this.speakers_items = res;
    });


  }
  f: any = {
    file: '',
    name: '',
    ext: '',
    type: ''
  }
  files: any[] = [];
  filesnum: any;

  firestore = firebase.storage();

  private chatsubscription: Subscription = new Subscription();
  private socialsubscription: Subscription = new Subscription();

  ngOnDestroy() {
    this.chatsubscription.unsubscribe();
    this.socialsubscription.unsubscribe();
  }

  ngOnInit() {
    this.event = this.eventsService.getCurrent();
    if (!this.event) {
      this.init();
    }  
     
    this.socialService.getSocials().subscribe(async res => {
      this.social_items = res.filter(i => i.link != ''); 
    });
  }

  init() {
    this.activatedRoute.params.subscribe((params) => {
      this.eventsService.getEvent(params.id).subscribe(async res => {
        this.event = event;
        this.updateMapElement();
      });
    });

    
  }

  ngAfterViewInit(): void {
    if (this.event) {
      this.updateMapElement();
    }
  }

  isInFavorites(key: string) {
    if (key == 'yes') {
      return true;
    } else {
      return false;
    }
    //return this.eventsService.isInFavorites(key);
  }

  toggleFavorites(event) {

    if (event.favorite == 'yes') {
      event.favorite = 'no';
    } else {
      event.favorite = 'yes';
    }

    this.eventsService.updateEvent(event, event.id);
  }

  private updateMapElement() {
    const mapData = {
      origin: {
        lat: -29.853935,
        lng: 31.026966,
        name: 'Durban ICC',
      },
      zoom: parseInt('18', 10),
    };

    const mapEle = this.mapElement.nativeElement;

    const map = new google.maps.Map(mapEle, {
      center: mapData.origin,
      zoom: mapData.zoom,
    });
    const marker = new google.maps.Marker({
      position: mapData.origin,
      map: map,
      title: mapData.origin.name,
    });
  }

  async deleteevent(id) {
    this.eventsService.removeEvent(id);

    const toast = await this.toastCtrl.create({
      message: 'Event deleted',
      duration: 3000,
    });
    toast.present();

    this.router.navigate(['tabs/events']);
  }

  async updateevent(event) {

    try { this.event.documents = this.files; } catch (ext) { this.event.documents = []; }

    if (typeof (this.event.documents) == 'undefined') {
      this.event.documents = [];
    }

    console.log(this.event)
    this.eventsService.updateEvent(event, event.id);

    const toast = await this.toastCtrl.create({
      message: 'Event updated',
      duration: 3000,
    });
    toast.present();
  }













  getAllIomage(image) {
    var img;
    new Promise((resolve, reject) => {
      this.firestore.ref('/pictures/').child(image).getDownloadURL().then((url) => {
        img = url;
      })
    })

    return img;
  }

  getPicture(sourceType) {
    const cameraOptions: CameraOptions = {
      quality: 50,
      destinationType: Camera.DestinationType.DATA_URL,
      encodingType: Camera.EncodingType.JPEG,
      mediaType: Camera.MediaType.PICTURE,
      sourceType: sourceType
    };

    Camera.getPicture(cameraOptions)
      .then((captureDataUrl) => {
        this.captureDataUrl = 'data:image/jpeg;base64,' + captureDataUrl;
      }, (err) => {
        console.log(err);
      });
  }


  upload(form: NgForm) {
    let storageRef = firebase.storage().ref();
    // Create a timestamp as filename
    const filename = Math.floor(Date.now() / 1000);

    // Create a reference to 'images/todays-date.jpg'
    const imageRef = storageRef.child(`pictures/${filename}.jpg`);
    this.addSpeaker(form, this.speaker, this.captureDataUrl);
    var d = imageRef.putString(this.captureDataUrl, firebase.storage.StringFormat.DATA_URL)
      .then((snapshot) => {
      });


  }

  async getImgFromServer(imgName: string) {
    let img;
    let dowvnloadIMG;
    var retVal = '';
    img = firebase.storage().ref("/pictures/" + imgName).getDownloadURL();
    let ref = firebase.storage().ref();
    const imgRef = ref.child("/pictures/" + imgName);
    const downloadURL = await imgRef.getDownloadURL()

    retVal = downloadURL;

    return retVal
  }

  async addsession(form: NgForm, session, filename) {

    this.session.sessions.push(this.eventsession)
    this.eventsService.updateEvent(this.session, this.session.id);

    const toast = await this.toastCtrl.create({
      message: 'Session added',
      duration: 3000,
    });
    toast.present();

    this.eventsession = { title: '', description: '', date: '', startdatetime: '', enddatetime: '', ratings: 0, rate: 0, rates: 0 }
  }


  async deletesession(title, start, end) {
    this.session.sessions = this.session.sessions.filter(i => i.title != title && i.startdatetime != start && i.enddatetime != end)
    this.sessions_items = this.session.sessions;
    this.eventsService.updateEvent(this.session, this.session.id);

    const toast = await this.toastCtrl.create({
      message: 'Session removed',
      duration: 3000,
    });
    toast.present();
 
  }
  

  async addSpeaker(form: NgForm, speaker, filename) {
    this.signupError = '';

    if (form.valid) {

      const toast = await this.toastCtrl.create({
        message: 'Speaker saved successfully',
        duration: 3000,
      });
      toast.present();

        return new Promise<any>((resolve, reject) => {
          this.afs.collection('/speakers').add({
            names: this.speaker.names,
            institution: this.speaker.institution,
            biography: this.speaker.biography,
            phone: this.speaker.phone,
            email: this.speaker.email,
            linkedin: this.speaker.linkedin,
            webpage: this.speaker.webpage,
            eventid: this.session.id,
            picture: this.captureDataUrl,
            rates: 0,
            ratings: 0,
          })
            .then(
              (res) => {
                resolve(res)
              },
              err => reject(err)
            )
        })


    }

    
  }





  async addSocial(form: NgForm) {
    this.signupError = '';

    if (form.valid) {

      const toast = await this.toastCtrl.create({
        message: 'Social saved successfully',
        duration: 3000,
      });
      toast.present();

      return new Promise<any>((resolve, reject) => {
        this.afs.collection('/socials').add({
          title: this.social.title,
          link:  this.social.link,
          type: this.social.type,
          date: new Date(), 
        })
          .then(
            (res) => {
              resolve(res)
            },
            err => reject(err)
          )
      })


    }


  }


  async deleteSpeaker(id) {
    this.speakerService.removeSpeaker(id);

    const toast = await this.toastCtrl.create({
      message: 'Speaker deleted successfully',
      duration: 3000,
    });
    toast.present();
  }



  async deleteSocial(id) {
    this.socialService.removeSocial(id);

    const toast = await this.toastCtrl.create({
      message: 'Social deleted successfully',
      duration: 3000,
    });
    toast.present();
  }




  sbaid: any;


  gotoFilePage(file) {
    cordova.InAppBrowser.open(file, "_system", "location=yes");
  }







  lol: any;

  selectFile(event) {
    let file
    this.FileChooser.open().then((uri) => {
      this.FilePath.resolveNativePath(uri).then((fileentry) => {
        let filename = this.eventsdata.getfilename(fileentry);
        let fileext = this.eventsdata.getfileext(fileentry);

        if (fileext == "pdf") {
          this.eventsdata.makeFileIntoBlob(fileentry, fileext, "application/pdf").then((fileblob) => {
            file = {
              blob: fileblob,
              type: "application/pdf",
              fileext: fileext,
              filename: filename
            }

            let storageRef = firebase.storage().ref();
            const filename_ = Math.floor(Date.now() / 1000);
            const fileRef = storageRef.child('files/' + filename_ + file.filename)
              //Saves the file to storage
              .put(file.blob, { contentType: file.type }).then(async (savedFile) => {
                //Gets the file url and saves it in the database
                this.files.push({
                  file: 'https://firebasestorage.googleapis.com/v0/b/vukati-mki.appspot.com/o/files%2F' + filename_ + file.filename + '?alt=media',
                  name: file.filename,
                  ext: file.fileext,
                  type: file.type
                });

                this.updateevent(event);

                const toast = await this.toastCtrl.create({
                  message: 'Document uploaded',
                  duration: 3000,
                });
                toast.present();
              })

          })
        }
        if (fileext == "docx") {
          this.eventsdata.makeFileIntoBlob(fileentry, fileext, "application/vnd.openxmlformats-officedocument.wordprocessingml.document").then((fileblob) => {
            file = {
              blob: fileblob,
              type: "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
              fileext: fileext,
              filename: filename
            }
            let storageRef = firebase.storage().ref();
            const filename_ = Math.floor(Date.now() / 1000);
            const fileRef = storageRef.child('files/' + filename_ + file.filename)
              //Saves the file to storage
              .put(file.blob, { contentType: file.type }).then(async (savedFile) => {
                //Gets the file url and saves it in the database
                this.files.push({
                  file: 'https://firebasestorage.googleapis.com/v0/b/vukati-mki.appspot.com/o/files%2F' + filename_ + file.filename + '?alt=media',
                  name: file.filename,
                  ext: file.fileext,
                  type: file.type
                });

                this.updateevent(event);

                const toast = await this.toastCtrl.create({
                  message: 'Document uploaded',
                  duration: 3000,
                });
                toast.present();
              })
          })
        }
        if (fileext == "doc") {
          this.eventsdata.makeFileIntoBlob(fileentry, fileext, "application/msword").then((fileblob) => {
            file = {
              blob: fileblob,
              type: "application/msword",
              fileext: fileext,
              filename: filename
            }
            let storageRef = firebase.storage().ref();
            const filename_ = Math.floor(Date.now() / 1000);
            const fileRef = storageRef.child('files/' + filename_ + file.filename)
              //Saves the file to storage
              .put(file.blob, { contentType: file.type }).then(async (savedFile) => {
                //Gets the file url and saves it in the database
                this.files.push({
                  file: 'https://firebasestorage.googleapis.com/v0/b/vukati-mki.appspot.com/o/files%2F' + filename_ + file.filename + '?alt=media',
                  name: file.filename,
                  ext: file.fileext,
                  type: file.type
                });

                this.updateevent(event);

                const toast = await this.toastCtrl.create({
                  message: 'Document uploaded',
                  duration: 3000,
                });
                toast.present();
              })
          })
        }
        if (fileext == "epub") {
          this.eventsdata.makeFileIntoBlob(fileentry, fileext, "application/octet-stream").then((fileblob) => {
            file = {
              blob: fileblob,
              type: "application/octet-stream",
              fileext: fileext,
              filename: filename
            }
            let storageRef = firebase.storage().ref();
            const filename_ = Math.floor(Date.now() / 1000);
            const fileRef = storageRef.child('files/' + filename_ + file.filename)
              //Saves the file to storage
              .put(file.blob, { contentType: file.type }).then(async (savedFile) => {
                //Gets the file url and saves it in the database
                this.files.push({
                  file: 'https://firebasestorage.googleapis.com/v0/b/vukati-mki.appspot.com/o/files%2F' + filename_ + file.filename + '?alt=media',
                  name: file.filename,
                  ext: file.fileext,
                  type: file.type
                });

                this.updateevent(event);

                const toast = await this.toastCtrl.create({
                  message: 'Document uploaded',
                  duration: 3000,
                });
                toast.present();
              })
          })
        }
        if (fileext == "accdb") {
          this.eventsdata.makeFileIntoBlob(fileentry, filename, "application/msaccess").then((fileblob) => {
            file = {
              blob: fileblob,
              type: "application/msaccess",
              fileext: fileext,
              filename: filename
            }
            let storageRef = firebase.storage().ref();
            const filename_ = Math.floor(Date.now() / 1000);
            const fileRef = storageRef.child('files/' + filename_ + file.filename)
              //Saves the file to storage
              .put(file.blob, { contentType: file.type }).then(async (savedFile) => {
                //Gets the file url and saves it in the database
                this.files.push({
                  file: 'https://firebasestorage.googleapis.com/v0/b/vukati-mki.appspot.com/o/files%2F' + filename_ + file.filename + '?alt=media',
                  name: file.filename,
                  ext: file.fileext,
                  type: file.type
                });

                this.updateevent(event);

                const toast = await this.toastCtrl.create({
                  message: 'Document uploaded',
                  duration: 3000,
                });
                toast.present();
              })
          })
        }
        if (fileext == "xlsx") {
          let storageRef = firebase.storage().ref();
          const filename_ = Math.floor(Date.now() / 1000);
          const fileRef = storageRef.child('files/' + filename_ + file.filename)
            //Saves the file to storage
            .put(file.blob, { contentType: file.type }).then(async (savedFile) => {
              //Gets the file url and saves it in the database
              this.files.push({
                file: 'https://firebasestorage.googleapis.com/v0/b/vukati-mki.appspot.com/o/files%2F' + filename_ + file.filename + '?alt=media',
                name: file.filename,
                ext: file.fileext,
                type: file.type
              });

              this.updateevent(event);

              const toast = await this.toastCtrl.create({
                message: 'Document uploaded',
                duration: 3000,
              });
              toast.present();
            
          })
        }
        if (fileext == "pptx") {
          this.eventsdata.makeFileIntoBlob(fileentry, filename, "application/powerpoint").then((fileblob) => {
            file = {
              blob: fileblob,
              type: "application/powerpoint",
              fileext: fileext,
              filename: filename
            }
            let storageRef = firebase.storage().ref();
            const filename_ = Math.floor(Date.now() / 1000);
            const fileRef = storageRef.child('files/' + filename_ + file.filename)
              //Saves the file to storage
              .put(file.blob, { contentType: file.type }).then(async (savedFile) => {
                //Gets the file url and saves it in the database
                this.files.push({
                  file: 'https://firebasestorage.googleapis.com/v0/b/vukati-mki.appspot.com/o/files%2F' + filename_ + file.filename + '?alt=media',
                  name: file.filename,
                  ext: file.fileext,
                  type: file.type
                });
                 
                this.updateevent(event);

                const toast = await this.toastCtrl.create({
                  message: 'Document uploaded',
                  duration: 3000,
                });
                toast.present();
              })
          })
        }
        //      else if (fileext!="doc"||"epub"||"xlsx"||"pdf"||"accdb"||"docx" ){

        //alert("Can't add "+  filename)

        //      }

      })
    })
  }



  openFile(url) {
    this.iab.create(url.file);
  }

}






