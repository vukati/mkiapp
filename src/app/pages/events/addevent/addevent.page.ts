import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { NavController } from '@ionic/angular';
import { UserDataService } from 'src/app/services/common/user-data.service';
import { UserOptions, EventOptions, EventSession } from 'src/app/interfaces/user-options';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { EventsService } from 'src/app/services/common/events.service';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { IonItemSliding, ToastController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import * as firebase from 'firebase';

@Component({
  selector: 'app-addevent',
  templateUrl: './addevent.page.html',
  styleUrls: ['./addevent.page.scss'],
})
export class AddEventPage implements OnInit {

  signup: EventOptions = {
    title: '', date: '', startdatetime: '', enddatetime: '', description: '', location: '', favorite: 'no', reminder: false, owner: '', id: '', sessions: [], documents: []};
  submitted = false;
  signupError: string;
  captureDataUrl: string = '';
  constructor(
    private navCtrl: NavController,
    private userData: UserDataService,
    private titleService: Title,
    private eventsService: EventsService,
    public afs: AngularFirestore,
    private toastCtrl: ToastController,
    private router: Router,
  ) { }

  ngOnInit() {
    this.titleService.setTitle('Add Event');
  }

  async onSignup(form: NgForm) {
    this.submitted = true;
    this.signupError = '';

    if (form.valid) {
      this.addUser(this.signup);
      
      const toast = await this.toastCtrl.create({
        message: 'Event added Successfully!',
        duration: 3000,
      });
      toast.present();

      this.router.navigate(['tabs/events']);
    }
  }

  addUser(signup) {
    if (this.captureDataUrl == '') {
      this.addUser2(signup);
    } else {
      let storageRef = firebase.storage().ref();
      // Create a timestamp as filename
      const filename = Math.floor(Date.now() / 1000);

      // Create a reference to 'images/todays-date.jpg'
      const imageRef = storageRef.child(`images/${filename}.jpg`);
      imageRef.putString(this.captureDataUrl, firebase.storage.StringFormat.DATA_URL)
        .then((snapshot) => {
          this.captureDataUrl = 'https://firebasestorage.googleapis.com/v0/b/' + storageRef.bucket + '/o/' + `images%2F${filename}.jpg` + '?alt=media';
          this.addUser2(signup);
        });  
    }
  }

  addUser2(signup) {
    return new Promise<any>((resolve, reject) => {
      this.afs.collection('/events').add({
        title: signup.title,
        location: signup.location,
        description: signup.description,
        date: new Date(),
        startdatetime: signup.startdatetime,
        enddatetime: signup.enddatetime,
        favorite: signup.favorite,
        reminder: signup.reminder,
        image: this.captureDataUrl,
        owner: '',
        rsvps: '',
        ratings: 0,
        rates: 0,
        sessions: signup.sessions,
      })
        .then(
          (res) => {
            resolve(res)
          },
          err => reject(err)
        )
    })
  }


  getPicture(sourceType) {
    const cameraOptions: CameraOptions = {
      quality: 50,
      destinationType: Camera.DestinationType.DATA_URL,
      encodingType: Camera.EncodingType.JPEG,
      mediaType: Camera.MediaType.PICTURE,
      sourceType: sourceType
    };

    Camera.getPicture(cameraOptions)
      .then((captureDataUrl) => {
        this.captureDataUrl = 'data:image/jpeg;base64,' + captureDataUrl;
      }, (err) => {
        console.log(err);
      });
  }

}
