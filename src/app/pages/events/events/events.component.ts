import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { IonItemSliding, ToastController } from '@ionic/angular';
import { EventService, Event } from 'src/app/services/events.service';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.scss'],
})
export class EventsComponent implements OnInit {

  queryText = '';
  segment: 'all' | 'yes' = 'all';
  events: any[] = [];
  events_items: Event[] = [];
  events_items2: Event[] = [];

  constructor(
    private eventsService: EventService,
    private router: Router,
    private route: ActivatedRoute,
    private toastCtrl: ToastController,
    private titleService: Title,
  ) { }

  ngOnInit() {
    this.titleService.setTitle('Events');
    this.updateList();
  }

  ionViewWillEnter() {
    this.updateList();
  }

  private chatsubscription3: Subscription = new Subscription();

  ngOnDestroy() {
    this.chatsubscription3.unsubscribe();
  }

  updateList() {
    
    this.chatsubscription3.unsubscribe();
    this.chatsubscription3 = this.eventsService.getEvents1().subscribe(async res => {
      this.events_items = res.filter(item => item.owner == '');
      this.events_items2 = res.filter(item => item.owner == '');;
    });

    

    if (this.segment == 'yes') {
      this.events_items = this.events_items.filter(item => item.favorite == this.segment);
    } else {
      this.events_items = this.events_items2;
    }

  }

  goToEventDetail(event: any) {
    this.eventsService.setCurrent(event);
    this.router.navigate([`${event.id}`], { relativeTo: this.route });
  }

  async addFavorite(slidingItem: IonItemSliding, id: string) {
    slidingItem.close();

    //this.eventsService.toggleFavorites(id);

    const toast = await this.toastCtrl.create({
      message: 'Event added to favorites',
      duration: 3000,
    });
    toast.present();

    this.updateList();
  }

  async removeFavorite(slidingItem: IonItemSliding, id: string) {
    //this.eventsService.toggleFavorites(id);
    slidingItem.close();

    const toast = await this.toastCtrl.create({
      message: 'Event removed from favorites',
      duration: 3000,
    });
    toast.present();

    this.updateList();
  }

  isInFavorites(key: string) {
    return true;
  }

  addevent() {
    this.router.navigate(['tabs/addevent']);
  }
}
